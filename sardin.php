<?php
/**
 * Plugin Name: Sardin
 * Plugin URI: https://framagit.org/lregnier/sardin/
 * Description: Ce plugin permet de : créer des événements (tournois de go, stage, autres...) ; publier un formulaire d'inscription ; gérer la liste des inscrits
 * Author: Laurent Regnier
 * Licence: GPL v3 or later
 * License URI: https://www.gnu.org/licenses/gpl-3.0.en.html
 */

if (!defined('ABSPATH')) exit;

// Conditionne l'envoi de mail : si true aucun mail n'est envoyé et le contenu
// des mails qui seraient envoyés est affiché ; voir
// inscription_form.php:sardin_send_confirmation_mail()
define('SARDIN_DEBUG', false);

// Quelques paramètres
global $wpdb;
define('SARDIN_CONFIG', array(
     // Les noms des tables SQL
     'tables' => array(
	  'tournaments' => $wpdb->prefix . "sardin_tournaments",
	  'items' => $wpdb->prefix . "sardin_items",
	  'categories' => $wpdb->prefix . "sardin_categories",
	  'tmnt_categories' => $wpdb->prefix . "sardin_tmnt_categories",
	  'tarifs' => $wpdb->prefix . "sardin_tarifs",
	  'participants' => $wpdb->prefix . "sardin_participants",
	  'groups' => $wpdb->prefix . "sardin_groups",
	  'registrations' => $wpdb->prefix . "sardin_registrations",
     ),
     // Le nom de la page d'admin d'un tournoi ; le %d reçoit l'id (clef
     // primaire) du tournoi
     'tournament' => array(
	  'admin_slug' => 'sardin_tournament_%d',
     ),
     'roles' => array(
	  // Les 'managers' : ces rôles ont tous les droits sur tous les
	  // tournois
	  'manage_tournaments' => array('editor', 'administrator'),
	  // Rôles pouvant éventuellement être promu organisateur d'un tournoi
	  'manage_tournament' => array('contributor', 'author',
				       'editor', 'administrator'),
     ),
     'caps' => array(
	  // Permission de 'manager' : créer des nouveaux tournois ou des
	  // nouvelles catégories
	  'tournaments' => 'sardin_manage_tournaments',
	  // Permission d'organisateur d'un tournoi, le %d reçoit l'id (clef
	  // primaire) du tournoi
	  'tournament' => 'sardin_manage_tournament_%d',
     ),
));

require_once(plugin_dir_path(__FILE__) . 'db.php');
require_once(plugin_dir_path(__FILE__) . 'security.php');

//// Activation/Désactivation

// Activation du plugin
function sardin_activate() {
     // Mise en place de la bd ;
     sardin_update_tables();

     // Mise en place des permissions globales
     sardin_create_capabilities();
}
register_activation_hook(__FILE__, 'sardin_activate');

// Désactivation du plugin
function sardin_deactivate() {
     // Nettoyage des permissions globales et par utilisateur
     sardin_drop_capabilities();
     sardin_drop_users_capabilities();
}
register_deactivation_hook(__FILE__, 'sardin_deactivate');

function sardin_uninstall() {
     // On vire tout
     sardin_drop_tables();
}
register_uninstall_hook(__FILE__, 'sardin_uninstall');

// Mise en place des menus
global $sardin_tournaments_submenus;
$sardin_tournaments_submenus = array();

function sardin_register_admin_menus() {
     global $wpdb;
     require_once(plugin_dir_path(__FILE__) . 'admin.php');

     $hooktournament = add_menu_page(
	  "Création d'un nouveau tournoi",
	  "Tournois",
	  SARDIN_CONFIG['caps']['tournaments'],
	  "sardin_create_tournament",
	  "sardin_create_tournament_page",
	  plugin_dir_url(__FILE__) . 'icon.png',
	  20);
     add_action('load-' . $hooktournament,
		'sardin_validate_create_tournament_form');

     $hookcreate = add_submenu_page(
	  "sardin_create_tournament",
	  "Création d'un nouveau tournoi",
	  "Nouveau tournoi",
	  SARDIN_CONFIG['caps']['tournaments'],
	  "sardin_create_tournament",
	  "sardin_create_tournament_page");
     add_action('load-' . $hookcreate,
		'sardin_validate_create_tournament_form');

     $hookcategories = add_submenu_page(
	  "sardin_create_tournament",
	  "Gestion des catégories de tarifs",
	  "Catégories de tarifs",
	  SARDIN_CONFIG['caps']['tournaments'],
	  "sardin_admin_categories",
	  "sardin_categories_page");
     add_action('load-' . $hookcategories, 'sardin_validate_categories_form');

     sardin_reset_tournaments_submenus();
}
add_action('admin_menu', 'sardin_register_admin_menus');

// (Re)construit la liste des tournois dans le menu d'administration
function sardin_reset_tournaments_submenus($reset_actions=true) {
     global $wpdb, $sardin_tournaments_submenus;

     foreach ($sardin_tournaments_submenus as $submenu) {
	  if ($reset_actions) {
	       remove_action($submenu['action'],
			     'sardin_validate_tournament_form');
	       remove_action($submenu['action'],
			     'sardin_validate_inscriptions');
	  }
	  remove_submenu_page('sardin_create_tournament', $submenu['slug']);
     }

     $sardin_tournaments_submenus = array();
     foreach ($wpdb->get_results(
	  "SELECT * FROM ". SARDIN_CONFIG['tables']['tournaments'] ."
           ORDER BY start DESC",
	  ARRAY_A) as $tournament) {
	  $slug = sprintf(SARDIN_CONFIG['tournament']['admin_slug'],
			  $tournament['tournament_id']);
	  $hookname = add_submenu_page(
	       "sardin_create_tournament",
	       "Modification du tournoi {$tournament['name']}",
	       $tournament['name'],
	       sprintf(SARDIN_CONFIG['caps']['tournament'],
		       $tournament['tournament_id']),
	       $slug,
	       "sardin_tournament_page");
	  if ($reset_actions) {
	       add_action("load-$hookname",
			  $tournament['published'] == 0
			  ? 'sardin_validate_tournament_form'
			  : 'sardin_validate_inscriptions');
	  }
	  $sardin_tournaments_submenus[] = array(
	       'slug' => $slug,
	       'action' => "load-$hookname");
     }
}

// Récupération de la page d'admin d'un tournoi
function sardin_admin_url($tournament, $echo=false) {
     $tournament_slug = sprintf(SARDIN_CONFIG['tournament']['admin_slug'],
				$tournament['tournament_id']);
     if (is_admin()) {
	  return menu_page_url($tournament_slug, $echo);
     } else {
	  return add_query_arg('page', $tournament_slug,
			       admin_url('admin.php'));
     }
}

// Resources propres aux pages d'admin de tournoi
function sardin_load_admin_resources($hook) {
     if (strpos($hook, 'sardin_') === false)
	  return;
     wp_enqueue_style('sardin_admin_style',
		      plugins_url('admin.css', __FILE__),
		      array(),
		      null);
     wp_enqueue_script('sardin_admin_script',
		       plugins_url('admin.js', __FILE__),
		       array('jquery'),
		       null);
}
add_action('admin_enqueue_scripts', 'sardin_load_admin_resources');

//// Shortcodes

// Tableau des tarifs d'un tournoi
add_shortcode('sardin_tarifs', 'sardin_tarifs_table_shortcode');
function sardin_tarifs_table_shortcode($input) {
     $atts = shortcode_atts(
	  array(
	       'tournament' => null,
	       'caption' => null),
	  $input);

     $tournament = sardin_get_tournament_or_notfound($atts['tournament']);
     require_once(plugin_dir_path(__FILE__) . 'tarifs.php');
     ob_start();
     sardin_tarifs_table($tournament, $atts['caption']);
     return ob_get_clean();
}

// Formulaire d'inscription
add_shortcode('sardin_inscription', 'sardin_inscription_form_shortcode');
function sardin_inscription_form_shortcode($input) {
     $atts = shortcode_atts(
	  array(
	       'tournament' => null,
	       'verbose' => 'yes'),
	  $input);

     $tournament = sardin_get_tournament_or_notfound($atts['tournament']);
     $verbose = in_array(strtolower($atts['verbose']),
			 array('oui', 'yes', 'true', 'ok'));
     require_once(plugin_dir_path(__FILE__) . 'inscription_form.php');
     ob_start();
     sardin_inscription_form($tournament, $verbose);
     return ob_get_clean();
}

// Liste d'inscriptions
add_shortcode('sardin_participants', 'sardin_participants_list_shortcode');
function sardin_participants_list_shortcode($input) {
     $atts = shortcode_atts(
	  array(
	       'tournament' => null,
	       'show_count' => 'yes'),
	  $input);
     $tournament = sardin_get_tournament_or_notfound($atts['tournament']);
     $show_count = in_array(strtolower($atts['show_count']),
			    array('oui', 'yes', 'true', 'ok'));
     require_once(plugin_dir_path(__FILE__) . 'participants_list.php');
     ob_start();
     sardin_participants_list($tournament, $show_count);
     return ob_get_clean();
}

function sardin_get_tournament_or_notfound($tournament_id) {
     if (!$tournament_id) {
	  sardin_not_found();
     }
     if (! $tournament = sardin_get_tournament($tournament_id)) {
	  sardin_not_found();
     }
     return $tournament;
}

// Les fichiers de style associés aux shortcode
function sardin_load_resources() {
     global $post;

     if (!$post) return;

     if (has_shortcode($post->post_content, 'sardin_inscription')) {
	  wp_enqueue_style('sardin_inscription_form_style',
		       plugins_url('inscription_form.css', __FILE__),
		       array(),
		       null);
     }

     if (has_shortcode($post->post_content, 'sardin_participants')) {
	  wp_enqueue_style('sardin_participants_list_style',
		       plugins_url('participants_list.css', __FILE__),
		       array(),
		       null);
     }

     if (has_shortcode($post->post_content, 'sardin_tarifs')) {
	  wp_enqueue_style('sardin_tarifs_table_style',
		       plugins_url('tarifs_table.css', __FILE__),
		       array(),
		       null);
     }
}
add_action('wp_enqueue_scripts', 'sardin_load_resources');

function sardin_not_found() {
     global $wp_query;
     $wp_query->set_404();
     status_header(404);
     nocache_headers();
     include( get_query_template( '404' ) );
     die();
}
