<?php
//// Affichage des pages d'administration de tournois, traitement des
//// formulaires
if (!defined('SARDIN_CONFIG')) exit;

//// Variables globales

// Tournoi créé (positionné dans sardin_validate_tournament())
global $sardin_tournament_created;
$sardin_tournament_created = null;

// Les paramètres d'une nouvelle formule d'hébergement tant que celle-ci n'a
// pas été validée
global $sardin_new_lodging;
$sardin_new_lodging = null;

// Idem pour les nouvelles catégories
global $sardin_new_category;
$sardin_new_category = null;

// Messages aux utilisateurs
global $sardin_tournaments_messages;
$sardin_tournaments_messages = array();

define('SARDIN_INIT_TOURNAMENT', array('tournament_id' => null,
				       'name' => '',
				       'contact_mail' => '',
				       'start' => '',
				       'end' => '',
				       'max_participants' => '',
				       'reg_end' => '',
				       'published' => 0,
				       'inscription_url' => ''));

//// Points d'entrée : fonctions d'affichage des formulaires (création,
//// administration d'un tournoi, administration des catégories)

// Création d'un tournoi ; si la requête est POST et la variable globale
// $sardin_tournament_created est positionnée, le tournoi vient d'être créé et
// on affiche la page de succès.
function sardin_create_tournament_page() {
     global $sardin_tournament_created;

     if (!sardin_is_manager())
	  return;

     $html = 'tournament_form.html';
     if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	  // Avons nous créé un nouveau tournoi ?
	  if ($sardin_tournament_created) {
	       // Oui, on renvoie sur la page de succès
	       $tournament = $sardin_tournament_created;
	       $tournament['admin_url'] = sardin_admin_url($tournament);
	       $html = 'tournament_created.html';
	  } else {
	       // Non (erreurs de validation), on réinitialise les
	       // champs du formulaire aux valeurs renvoyées
	       $tournament = array('tournament_id' => null);
	       foreach (array('name', 'start', 'end',
			      'contact_mail', 'max_participants', 'reg_end')
			as $key)
		    $tournament[$key] =
			sanitize_text_field(@$_POST["sardin_tournament_$key"]);
	  }
     } else {
	  $tournament = SARDIN_INIT_TOURNAMENT;
     }
     include(plugin_dir_path(__FILE__) . $html);
}

// Administration d'un tournoi : aiguillage selon que le tournoi est publié ou
// pas
function sardin_tournament_page() {
     $tournament = sardin_get_tournament_from_page();
     if (!$tournament) {
	  return;
     }

     if (!sardin_is_organisor($tournament)) {
	  return;
     }

     if ($tournament['published'] == 0) {
	  // Tournoi en cours d'édition
	  return sardin_tournament_form($tournament);

     } else {
	  // Tournoi publié
	  require_once(plugin_dir_path(__FILE__) . 'participants_list.php');
	  return sardin_participants_list($tournament);
     }
}

// Édition d'un tournoi existant ; le formulaire affiche plus de champs que
// celui de création : organisateurs, hébergements et options.
function sardin_tournament_form($tournament) {
     // Préparation des dates pour affichages
     foreach (array('start', 'end', 'reg_end') as $key) {
	  $tournament[$key] = sardin_html_date($tournament[$key]);
     }

     //// Initialisation des champs à afficher
     // Liste des utilisateurs aggréés, organisateurs ou organisateurs
     // potentiels
     $users = array();
     foreach(sardin_authorized_members() as $user) {
	  $organisor = sardin_is_organisor($tournament, $user);
	  $superuser = sardin_is_manager($user);
	  $users[] = array('user_id' => $user->ID,
			   'organisor' => $organisor,
			   'removable' => (!$superuser
					   && wp_get_current_user() != $user),
			   'pseudo' => $user->user_login);
     }

     $categories = sardin_get_categories($tournament);
     $lodgings = sardin_get_lodgings($tournament, $categories);
     $options = sardin_get_options($tournament, $categories);

     // ... et affichage ; $sardin_new_lodging a été renseignée par la fonction
     // de validation et contient possiblement des valeurs incomplètes que l'on
     // réaffiche pour le confort de l'utilisateur
     global $sardin_new_lodging;
     include(plugin_dir_path(__FILE__) . 'tournament_form.html');
}

// Validations ; fonctions appelées avant les fonctions d'affichage ci-dessus.

// Validation de la création d'un tournoi ; en cas de succès on positionne la
// variable globale $sardin_tournament_created pour notifier
// sardin_create_tournament_page()
function sardin_validate_create_tournament_form() {
     global $sardin_tournament_created;

     if ($_SERVER['REQUEST_METHOD'] != 'POST'
	 || !sardin_is_manager()) {
	  return;
     }

     $tournament = sardin_validate_tournament(SARDIN_INIT_TOURNAMENT);

     if ($tournament) {
	  // C'est un succès, un nouveau tournoi est né !
	  // Activation de la catégorie tarif normal
	  sardin_enable_category($tournament);

	  // Mise-à-jour des permissions et menus d'administration
	  require_once(plugin_dir_path(__FILE__). 'security.php');
	  sardin_create_tournament_capability($tournament);
	  sardin_reset_tournaments_submenus(false);
	  $sardin_tournament_created = $tournament;
	  return true;
     }
     return;
}

// Mise-à-jour d'un tournoi non publié (cf
// sardin.php:sardin_reset_tournaments_submenus())
function sardin_validate_tournament_form() {
     if ($_SERVER['REQUEST_METHOD'] != 'POST'
	 || !sardin_is_manager()) {
	  return;
     }

     if (! $tournament = sardin_get_tournament_from_page()) return;
     if (!sardin_is_organisor($tournament)) {
	  return;
     }

     // Si le nom du tournoi a changé il faudra refaire les menus d'admin
     $name_orig = $tournament['name'];

     $tournament = sardin_validate_tournament($tournament);
     if (!$tournament) {
	  return;
     }

     if ($tournament['name'] != $name_orig) {
	  // Le titre a changé, on refait les menus d'admin
	  sardin_reset_tournaments_submenus(false);
     }

     // Validation du reste
     $categories = sardin_validate_enabled_categories($tournament);
     if ($categories) {
	  require_once(plugin_dir_path(__FILE__). 'security.php');
	  $result = (sardin_validate_organisors($tournament)
		     && sardin_validate_lodgings($tournament, $categories)
		     && sardin_validate_options($tournament, $categories));
     } else {
	  $result = false;
     }

     return $result;
}

// Validation du tournoi, fonction commune à la création et la mise-à-jour : en
// cas de succès, sauvegarde dans la base et retourne le nouveau tournoi, null
// en cas d'échec.
function sardin_validate_tournament($tournament) {
     global $wpdb;

     $tournament_id = $tournament['tournament_id'];
     $errors = false;

     // C'est parti. Le nom :
     $name = sanitize_text_field(stripslashes(
	  $_POST['sardin_tournament_name']));
     if (!$name) {
	  sardin_notify("Entrer le nom du tournoi", 'error');
	  $errors = true;
     } elseif (mb_strtolower($name) != mb_strtolower($tournament['name'])) {
	  $query = $wpdb->prepare(
	       "SELECT COUNT(*)
                  FROM ". SARDIN_CONFIG['tables']['tournaments'] ."
                 WHERE name LIKE %s",
	       $name);
	  $count = $wpdb->get_var($query);
	  if ($count > 0) {
	       sardin_notify("Un tournoi de ce nom existe déjà", 'error');
	       $errors = true;
	  } else {
	       $tournament['name'] = $name;
	  }
     }

     // Les dates
     foreach (array('start' => 'début', 'end' => 'fin',
		    'reg_end' => "fin des inscriptions")
	      as $key => $libel) {
	  $date = sanitize_text_field($_POST["sardin_tournament_$key"]);
	  if (!$date) {
	       if ($key == 'reg_end') {
		    $tournament['reg_end'] = null;
	       } else {
		    sardin_notify("Entrer la date de $libel du tournoi",
				  'error');
		    $errors = true;
	       }
	  } elseif (preg_match('<^[0-9]{1,2}[/-][0-9]{1,2}[/-][0-9]{4}$>',
			     $date) != 1) {
	       sardin_notify("Date de $libel invalide&nbsp;; entrer les dates au format 'jj/mm/aaaa' ou 'jj-mm-aaaa'", 'error');
	       $errors = true;
	  } else {
	       $tournament[$key] = sardin_sql_date($date);
	  }
     }

     if ($tournament['start'] and $tournament['end'] and
	 $tournament['end'] < $tournament['start']) {
	       sardin_notify("La fin précède le début !", 'error');
	       $errors = true;
     }

     // Email de contact
     $input_mail = @$_POST['sardin_tournament_contact_mail'];
     if (!$input_mail) {
	  sardin_notify("Spécifier une adresse de contact");
	  $errors = true;
     } else {
	  $tournament['contact_mail'] = sanitize_email($input_mail);
	  if ($tournament['contact_mail'] != $input_mail) {
	       sardin_notify("L'adresse de contact est invalide");
	       $errors = true;
	  }
     }

     // Nombre de participants
     $max = sanitize_text_field($_POST['sardin_tournament_max_participants']);
     if ($max) {
	  $max = intval($max);
	  if ($max <= 0) {
		    sardin_notify(
			 "Valeur incorrecte du nombre maximum de participants",
			 'error');
		    $errors = true;
	  } else
	       $tournament['max_participants'] = $max;

     } else
	  $tournament['max_participants'] = null;

     // Validation terminée, on passe à la sauvegarde
     if ($errors) {
	  return;
     }

     // Pas d'erreur : on sauve
     if ($tournament_id) {
	  // Le tournoi existait, c'est une mise-à-jour
	  if (isset($_POST['sardin_publish_tournament'])) {
	       $tournament['published'] = 1;
	  }
	  $updated = $wpdb->update(
	       SARDIN_CONFIG['tables']['tournaments'],
	       array_slice($tournament, 1),
	       array('tournament_id' => $tournament_id),
	       array("%s", "%s", "%s", "%s", "%d", "%s", "%d"),
	       array("%d"));
	  if ($updated === false) {
	       sardin_notify(
		    "Tournoi : échec de la mise-à-jour", 'error');
	       return;
	  } elseif ($updated == 1) {
	       sardin_notify("Tournoi : mise-à-jour effectuée");
	  } elseif ($updated > 1) {
	       sardin_notify(
		    "Tournoi : pb dans la mise-à-jour, base de données corrompue ?",
		    'error');
	       return;
	  }
     } else {
	  // Le tournoi n'existait pas, c'est une création
	  if ($wpdb->insert(
	            SARDIN_CONFIG['tables']['tournaments'],
	            array_slice($tournament, 1),
	            array("%s", "%s", "%s", "%s", "%d", "%s", "%d")) == 1) {
	       $tournament_id = $wpdb->insert_id;
	       $tournament['tournament_id'] = $tournament_id;
	  } else {
	       sardin_notify("Échec de la création", 'error');
	       return;
	  }
     }
     return $tournament;
}

// Organisateurs
function sardin_validate_organisors($tournament) {
     $input_organisors = @$_POST['sardin_organisors'];
     $tournament_id = $tournament['tournament_id'];
     if (!$tournament_id) {
	  return;
     }

     foreach (sardin_authorized_members() as $user) {
	  if (sardin_is_manager($user)
	      // on ne peut pas se supprimer soit-même de l'organisation
	      || $user == wp_get_current_user()) {
	       continue;
	  }
	  if (@$input_organisors[$user->ID] == 'organisor') {
	       if (sardin_give_tournament_capability($tournament, $user)) {
		    sardin_notify(
			 "{$user->user_login} ajouté aux organisateurs");
	       }
	  } elseif (sardin_remove_tournament_capability($tournament, $user)) {
	       sardin_notify("{$user->user_login} supprimé des organisateurs");
	  }
     }
     return true;
}

// Catégories actives
function sardin_validate_enabled_categories($tournament) {
     $categories = sardin_get_categories($tournament);
     $input_categories = @$_POST['sardin_enabled_categories'];
     foreach ($categories as &$category) {
	  $input_category = @$input_categories[$category['code']];
	  if ($input_category == 'enabled') {
	       if (!$category['enabled']) {
		    sardin_enable_category($tournament, $category);
		    $category['enabled'] = true;
		    sardin_notify("Tournoi : tarif {$category['name']} activé");
	       }
	  } elseif ($category['enabled']) {
	       sardin_enable_category($tournament, $category, false);
	       $category['enabled'] = false;
	       sardin_notify("Tournoi : tarif {$category['name']} désactivé");
	  }
     }
     return $categories;
}

// Formules d'hébergement ; grosse fonction : on valide les différentes
// formules existantes, leurs tarifs, puis une éventuelle nouvelle formule et
// ses tarifs
function sardin_validate_lodgings($tournament, $categories) {
     global $wpdb;

     $lodgings = sardin_get_lodgings($tournament, $categories);
     $errors = false;
     $input_deletes = @$_POST['sardin_lodgings_deletes'];
     $input_names = @$_POST['sardin_lodgings_names'];
     $input_durations = @$_POST['sardin_lodgings_durations'];
     $input_tarifs = array();
     $updates = 0;
     $deletes = 0;

     // Allons-y
     foreach ($lodgings as $lodging) {
	  // Suppression ?
	  if (@$input_deletes[$lodging['item_id']] == 'delete') {
	       sardin_delete_option($lodging);
	       sardin_notify(
		    "Hébergement {$lodging['name']} supprimé");
	       $deletes += 1;
	       continue;
	  }

	  // Validation : la durée
	  $duration = @$input_durations[$lodging['item_id']];
	  if (!$duration) {
	       sardin_notify("Hébergement : entrer une durée (non nulle) pour chaque type d'hébergement.",
			     'error');
	       $errors = true;
	       continue;
	  } else {
	       $duration = sanitize_text_field($duration);
	       if (!is_numeric($duration)) {
		    sardin_notify("Hébergement : les durées doivent être des nombres (de jours)",
				  'error');
		    $errors = true;
		    continue;
	       }
	       $duration = intval($duration);
	       if ($duration <= 0) {
		    sardin_notify("Hébergement : les durées doivent être plus grande que 1", 'error');
		    $errors = true;
		    continue;
	       }
	  }

	  // Le nom
	  $name = @sanitize_text_field(stripslashes(
	       $input_names[$lodging['item_id']]));
	  if (!$name) {
	       sardin_notify("Hébergement : indiquer un nom pour chaque type d'hébergement",
			     'error');
	       $errors = true;
	       continue;
	  } elseif (mb_strtolower($name) != mb_strtolower($lodging['name'])
		    || $duration != $lodging['duration']) {
	       $query = $wpdb->prepare(
		    "SELECT COUNT(*) FROM ". SARDIN_CONFIG['tables']['items'] ."
                      WHERE tournament_id = %d
                        AND name like %s
                        AND duration = %d
                        AND type = 'lodging'",
		    $tournament['tournament_id'], $name, $duration);
	       if ($wpdb->get_var($query) > 0) {
		    sardin_notify("Hébergement : une formule de ce type et de cette durée existe déjà", 'error');
		    $errors = true;
		    continue;
	       }
	  }
	  $lodging['name'] = $name;
	  $lodging['duration'] = $duration;

	  // C'est bon, on sauve
	  $update = $wpdb->update(SARDIN_CONFIG['tables']['items'],
				  array('name' => $name,
					'duration' => $duration),
				  array('item_id' => $lodging['item_id']),
				  array("%s", "%d"),
				  array("%d"));
	  if ($update === false) {
	       sardin_notify("Hébergement {$lodging['name']} : échec de la mise-à-jour", 'error');
	       $errors = true;
	  } else {
	       $updates += $update;
	  }

	  // Les tarifs maintenant ; on est un peu laxiste, en cas d'absence ou
	  // de mauvaise entrée on initialise à 0 sans signaler d'erreur
	  $input_tarifs[$lodging['item_id']] =
		      $_POST["sardin_lodgings_tarifs:{$lodging['item_id']}"];
	  foreach ($lodging['tarifs'] as $code => $tarif) {
	       if (!$tarif['enabled']) continue;
	       if (@$input_tarifs[$lodging['item_id']][$code]) {
		    $input_val = floatval(
			 $input_tarifs[$lodging['item_id']][$code]);
	       } elseif (@$lodging['tarifs'][$code]['value']) {
		    $input_val = $lodging['tarifs'][$code]['value'];
	       } else {
		    $input_val = 0;
	       }
	       $category_id = $categories[$code]['category_id'];
	       if ($tarif['value'] === null) {
		    if (!$wpdb->insert(
			 SARDIN_CONFIG['tables']['tarifs'],
			 array('item_id' => $lodging['item_id'],
			       'category_id' => $category_id,
			       'value' => $input_val),
			 array("%d", "%d", "%f"))) {
			 sardin_notify("Hébergement {$lodging['name']} : échec de la création du tarif $code", 'error');
			 $errors = true;
		    } else {
			 $update += 1;
		    }
	       } else {
		    $update = $wpdb->update(
			 SARDIN_CONFIG['tables']['tarifs'],
			 array('value' => $input_val),
			 array('item_id'=> $lodging['item_id'],
			       'category_id' => $category_id),
			 array("%d"),
			 array("%d", "%f"));
		    if ($update === false) {
			 sardin_notify("Hébergement {$lodging['name']} : échec de la mise-à-jour du tarif $code");
			 $errors = true;
		    } else {
			 $updates += $update;
		    }
	       }
	  }
     }

     if (!$errors && $updates > 0) {
	  sardin_notify("Hébergement : mises-à-jour effectuées");
     }

     // Y a-t-il une nouvelle formule à créer ?
     // La variable globale
     // $sardin_new_lodging conserve les valeurs des champs pour réaffichage
     global $sardin_new_lodging;
     $sardin_new_lodging = @$_POST['sardin_new_lodging'];
     $sardin_new_lodging['name'] = sanitize_text_field(stripslashes(
	  @$sardin_new_lodging['name']));
     if (!$sardin_new_lodging['name']) {
	  // Non : on a fini
	  return !$errors;
     }

     // Oui, on la valide
     $sardin_new_lodging['duration'] = sanitize_text_field(
	  @$sardin_new_lodging['duration']);
     if ($sardin_new_lodging['duration'] === null) {
	  sardin_notify("Hébergement : entrer une durée (non nulle) pour la nouvelle formule d'hébergement",
			'error');
	  return false;
     }
     $sardin_new_lodging['duration'] = intval($sardin_new_lodging['duration']);
     if ($sardin_new_lodging['duration'] <= 0) {
	  sardin_notify("Hébergement : entrer une durée (en nombre de nuitées) plus grande que 1 pour la nouvelle formule", 'error');
	  return false;
     }

     $query = $wpdb->prepare("SELECT COUNT(*)
                                FROM ". SARDIN_CONFIG['tables']['items'] ."
                               WHERE name LIKE %s
                                 AND duration = %d
                                 AND tournament_id = %d",
			     $sardin_new_lodging['name'],
			     $sardin_new_lodging['duration'],
			     $tournament['tournament_id']);
     if ($wpdb->get_var($query) > 0) {
	  sardin_notify("Hébergement : une formule de ce type et de cette durée existe déjà pour ce tournoi",
			'error');
	  return false;
     }


     // C'est bon, on sauve
     if (!$wpdb->insert(SARDIN_CONFIG['tables']['items'],
			array('tournament_id' => $tournament['tournament_id'],
			      'name' => $sardin_new_lodging['name'],
			      'duration' => $sardin_new_lodging['duration']),
			array("%d", "%s", "%d"))) {
	  sardin_notify("Hébergement : échec de la création d'une nouvelle formule", 'error');
	  return false;
     }

     $new_lodging_id = $wpdb->insert_id;
     // On annule $sardin_new_lodging pour ne pas réafficher les champs.
     $sardin_new_lodging = null;
     sardin_notify("Hébergement : nouvelle formule ajoutée");

     // Et on valide les tarifs de la nouvelle formule
     $input_new_lodging_tarifs = @$_POST['sardin_new_lodging_tarifs'];
     if (!$input_new_lodging_tarifs) {
	  sardin_notify("Hébergement : entrer les tarifs de la nouvelle formule", 'error');
	       return false;
     }

     foreach ($categories as $code => $category) {
	  if (!$category['enabled']) continue;
	  $new_tarif = sanitize_text_field(@$input_new_lodging_tarifs[$code]);
	  $new_tarif = floatval($new_tarif);
	  if (!$wpdb->insert(SARDIN_CONFIG['tables']['tarifs'],
			     array('item_id' => $new_lodging_id,
				   'category_id' => $category['category_id'],
				   'value' => $new_tarif),
			     array("%d", "%d", "%f"))) {
	       sardin_notify("Hébergement : échec de la création du tarif $code pour la nouvelle formule", 'error');
	       return false;
	  }
     }
     return true;
}

// Validation des options, essentiellement la même fonction que pour les
// hébergements mais en légèrement plus simple
function sardin_validate_options($tournament, $categories) {
     global $wpdb;

     $options = sardin_get_options($tournament, $categories);
     $errors = false;
     $input_deletes = @$_POST['sardin_options_deletes'];
     $input_names = @$_POST['sardin_options_names'];
     $input_priorities = @$_POST['sardin_options_priorities'];
     $input_tarifs = array();
     $updates = 0;
     $deletes = 0;

     foreach ($options as $option) {
	  if (@$input_deletes[$option['item_id']] == 'delete') {
	       sardin_delete_option($option);
	       sardin_notify(
		    "Options : {$option['name']} supprimé");
	       $deletes += 1;
	       continue;
	  }

	  $name = @sanitize_text_field(stripslashes(
	       $input_names[$option['item_id']]));
	  if (!$name) {
	       sardin_notify("Options : indiquer un nom pour chaque option",
			     'error');
	       $errors = true;
	       continue;
	  }  elseif (mb_strtolower($name) != mb_strtolower($option['name'])) {
	       $query = $wpdb->prepare(
		    "SELECT COUNT(*) FROM ". SARDIN_CONFIG['tables']['items'] ."
                      WHERE tournament_id = %d
                        AND name like %s
                        AND type = 'option'",
		    $tournament['tournament_id'], $name);
	       if ($wpdb->get_var($query) > 0) {
		    sardin_notify("Options : une option de ce nom existe déjà",
				  'error');
		    $errors = true;
		    continue;
	       }
	  }
	  $option['name'] = $name;

	  $priority = @$input_priorities[$option['item_id']];
	  if (!$priority) {
	       $priority = 500;
	  } else {
	       $priority = sanitize_text_field($priority);
	       if (!is_numeric($priority)) {
		    sardin_notify("Options : les priorités doivent être des nombres",
				  'error');
		    $errors = true;
		    continue;
	       }
	       $priority = intval($priority);
	       if ($priority < 0 || $priority > 1000) {
		    sardin_notify("Options : les priorités doivent être comprises entre 0 et 1000",
				  'error');
		    $errors = true;
		    continue;
	       }
	  }
	  $option['duration'] = $priority;
	  $update = $wpdb->update(SARDIN_CONFIG['tables']['items'],
				  array('name' => $name,
					'duration' => $priority),
				  array('item_id' => $option['item_id']),
				  array("%s", "%d"),
				  array("%d"));
	  if ($update === false) {
	       sardin_notify("Options :  {$option['name']} : échec de la mise-à-jour", 'error');
	       $errors = true;
	  } else {
	       $updates += $update;
	  }

	  $input_tarifs[$option['item_id']] =
		      $_POST["sardin_options_tarifs:{$option['item_id']}"];
	  foreach ($option['tarifs'] as $code => $tarif) {
	       if (!$tarif['enabled']) continue;
	       if (@$input_tarifs[$option['item_id']][$code]) {
		    $input_val = floatval(
			 $input_tarifs[$option['item_id']][$code]);
	       } elseif (@$option['tarifs'][$code]['value']) {
		    $input_val = $option['tarifs'][$code]['value'];
	       } else {
		    $input_val = 0;
	       }
	       $category_id = $categories[$code]['category_id'];
	       if ($tarif['value'] === null) {
		    if (!$wpdb->insert(
			 SARDIN_CONFIG['tables']['tarifs'],
			 array('item_id' => $option['item_id'],
			       'category_id' => $category_id,
			       'value' => $input_val),
			 array("%d", "%d", "%f"))) {
			 sardin_notify("Options : {$option['name']} : échec de la création du tarif $code", 'error');
			 $errors = true;
		    } else {
			 $update += 1;
		    }
	       } else {
		    $update = $wpdb->update(
			 SARDIN_CONFIG['tables']['tarifs'],
			 array('value' => $input_val),
			 array('item_id'=> $option['item_id'],
			       'category_id' => $category_id),
			 array("%d"),
			 array("%d", "%f"));
		    if ($update === false) {
			 sardin_notify("Options : {$option['name']} : échec de la mise-à-jour du tarif $code");
			 $errors = true;
		    } else {
			 $updates += $update;
		    }
	       }
	  }
     }

     if (!$errors && $updates > 0) {
	  sardin_notify("Options : mises-à-jour effectuées");
     }

     $new_option_name = sanitize_text_field(stripslashes(
	  @$_POST['sardin_new_option_name']));
     if (!$new_option_name) {
	  return !$errors;
     } else {
	  $query = $wpdb->prepare(
	       "SELECT COUNT(*)
                  FROM ". SARDIN_CONFIG['tables']['items'] ."
                 WHERE name LIKE %s
                   AND tournament_id = %d",
	       $new_option_name, $tournament['tournament_id']);
	  if ($wpdb->get_var($query) > 0) {
	       sardin_notify("Options: une formule de ce nom existe déjà pour ce tournoi", 'error');
	       return false;
	  }
     }

     $new_option_priority = @$_POST['sardin_new_option_priority'];
     if (!$new_option_priority) {
	  $new_option_priority = 500;
     } elseif (!is_numeric($new_option_priority)) {
	  sardin_notify("Options : les priorités doivent être des nombres (compris entre 0 et 1000)", 'error');
	  return false;
     } else {
	  $new_option_priority = intval($new_option_priority);
     }
     if ($new_option_priority < 0 || $new_option_priority > 1000) {
	  sardin_notify("Options : entrer une priorité comprise entre 0 et 1000 à la nouvelle option",
			'error');
	  return false;
     }
     if (!$wpdb->insert(SARDIN_CONFIG['tables']['items'],
			array('tournament_id' => $tournament['tournament_id'],
			      'name' => $new_option_name,
			      'type' => 'option',
			      'duration' => $new_option_priority),
			     array("%d", "%s", "%s", "%d"))) {
	  sardin_notify("Options : échec de la création d'une nouvelle option",
			'error');
	       return false;
     }
     $new_option_id = $wpdb->insert_id;
     sardin_notify("Options : nouvelle option ajoutée");

     $input_new_option_tarifs = @$_POST['sardin_new_option_tarifs'];
     if (!$input_new_option_tarifs) {
	  sardin_notify("Options : entrer les tarifs de la nouvelle option",
			'error');
	       return false;
     }
     foreach ($categories as $code => $category) {
	  if (!$category['enabled']) continue;
	  $new_tarif = sanitize_text_field(@$input_new_option_tarifs[$code]);
	  $new_tarif = intval($new_tarif);
	  if (!$wpdb->insert(SARDIN_CONFIG['tables']['tarifs'],
			     array('item_id' => $new_option_id,
				   'category_id' => $category['category_id'],
				   'value' => $new_tarif),
			     array("%d", "%d", "%f"))) {
	       sardin_notify("Options : échec de la création du tarif $code pour la nouvelle option", 'error');
	       return false;
	  }
     }
     return true;
}

// Fonction de validation appelée lorsque le tournoi est publié (cf
// sardin.php:sardin_reset_tournaments_submenus())
function sardin_validate_inscriptions() {
     global $wpdb;

     $tournament = sardin_get_tournament_from_page();
     if (!$tournament) {
	  return;
     }
     if (isset($_POST['sardin_unpublish'])) {
	  $wpdb->update(SARDIN_CONFIG['tables']['tournaments'],
			array('published' => 0),
			array('tournament_id' => $tournament['tournament_id']),
			array("%d"),
			array("%d")
	  );
     } elseif (isset($_POST['sardin_export_inscriptions'])) {
	  if (!sardin_export_inscriptions($tournament)) {
	       sardin_notify("Échec de l'export...");
	  }
     }
     return true;
}

// Cette fonction doit être appelée avant tout sortie vers le client ; si tout
// se passe bien elle répond et termine le traitement de la requête courante
// (appel à exit()).
function sardin_export_inscriptions($tournament) {
     $inscriptions = sardin_get_participants_with_options($tournament);
     $options = sardin_get_options($tournament);
     $fields = array('Nom', "Date d'inscription", "Dernière modification",
		     'Type tarif', 'Code tarif',
		     'Hébergement', "Type d'hébergement",
		     'Arrivée', 'Durée', 'Tarif hébergement');
     foreach ($options as $option) {
	  $fields[] = $option['name'];
	  $fields[] = "Date insc {$option['name']}";
	  $fields[] = "Date mod {$option['name']}";
	  $fields[] = "Tarif {$option['name']}";
     }
     $fields = array_merge($fields,
			   array('groupe', 'email', 'téléphone', 'remarques'));

     // 1ère ligne du csv : les en-têtes de colonnes
     $csv = sardin_csv_line($fields);

     // Et maintenant les inscrits
     $groups = array();
     foreach ($inscriptions as $ppant) {
	  $line = array($ppant['name'],
			$ppant['reg_date'],
			$ppant['mod_date'],
			$ppant['catname'],
			$ppant['catcode'],
			$ppant['lodging_id'],
			$ppant['lodging_name'],
			$ppant['arrival'], $ppant['duration'],
			$ppant['lodging_fee']
	  );
	  foreach ($options as $option) {
	       $op_id = $option['item_id'];
	       if (array_key_exists($op_id, $ppant['options'])) {
		    $line = array_merge(
			 $line,
			 array('oui',
			       $ppant['options'][$op_id]['reg_date'],
			       $ppant['options'][$op_id]['mod_date'],
			       $ppant['options'][$op_id]['fee']));
	       } else {
		    $line = array_merge($line, array('non', '', '', ''));
	       }
	  }
	  $line[] = $ppant['group_id'];
	  if (array_key_exists($ppant['group_id'], $groups)) {
	       $line = array_merge($line, array('', '', ''));
	  } else {
	       $line = array_merge(
		    $line,
		    array($ppant['email'], $ppant['phone'], $ppant['comment']));
	       $groups[$ppant['group_id']] = true;
	  }
	  $csv .= "\n" . sardin_csv_line($line);
     }

     header("Content-Type: text/csv; charset=utf-8");
     header("Content-Length: ". strlen($csv));
     header("Content-Disposition: attachment; filename=inscriptions.csv");
     print $csv;
     exit;
}

function sardin_csv_line($fields) {
     foreach ($fields as &$field) {
	  if (is_numeric($field)) {
	       continue;
	  } elseif ($field && is_string($field)) {
	       $field = str_replace('"', "''", $field);
	       $field = "\"$field\"";
	  } else {
	       $field = '';
	  }
     }
     return implode(',', $fields);
}

// Formulaire de gestion des catégories de tarifs
function sardin_categories_page() {
     global $sardin_new_category;
     $categories = sardin_get_categories();
     include(plugin_dir_path(__FILE__) . 'categories_form.html');
}

// Validation des catégories de tarifs
function sardin_validate_categories_form() {
     global $wpdb;

     if ($_SERVER['REQUEST_METHOD'] != 'POST'
	 || !sardin_is_manager()) {
	  return;
     }

     $categories = sardin_get_categories();
     $categories_table = SARDIN_CONFIG['tables']['categories'];
     $input_enabled = isset($_POST['sardin_categories_enabled'])
		    ? $_POST['sardin_categories_enabled']
		    : array();
     $input_names = isset($_POST['sardin_categories_names'])
		  ? $_POST['sardin_categories_names']
		  : array();
     $input_codes = isset($_POST['sardin_categories_codes'])
		  ? $_POST['sardin_categories_codes']
		  : array();
     $input_priorities = isset($_POST['sardin_categories_priorities'])
		  ? $_POST['sardin_categories_priorities']
		  : array();
     $modifs = 0;

     // On ne touche pas à la catégorie 1
     foreach (array_slice($categories, 1) as $category) {
	  $updates = array();
	  $updates_fmt = array();
	  $category_id = $category['category_id'];

	  $name = @sanitize_text_field($input_names[$category_id]);
	  if (!$name) {
	       sardin_notify("Entrer un nom pour chaque catégorie");
	       continue;
	  }
	  if (mb_strtolower($name) != mb_strtolower($category['name'])) {
	       $query = $wpdb->prepare(
		    "SELECT COUNT(*) FROM $categories_table
                      WHERE name LIKE %s",
		    $name);
	       $count = $wpdb->get_var($query);
	       if ($count > 0) {
		    sardin_notify(
			 "Catégorie '$name' : ce nom est déjà pris");
		    continue;
	       }
	  }
	  $updates['name'] = $name;
	  $updates_fmt[] = "%s";

	  $code = sanitize_text_field(@$input_codes[$category_id]);
	  if (!$code) {
	       sardin_notify("Entrer un code pour chaque catégorie");
	       continue;
	  } elseif ($code != $category['code']) {
	       $query = $wpdb->prepare(
		    "SELECT COUNT(*) FROM $categories_table
                      WHERE code LIKE %s",
		    $code);
	       $count = $wpdb->get_var($query);
	       if ($count > 0) {
		    sardin_notify(
			 "Catégorie '$name' : ce code est déjà pris");
		    continue;
	       }
	  }
	  $updates['code'] = $code;
	  $updates_fmt[] = "%s";

	  $priority = sanitize_text_field(@$input_priorities[$category_id]);
	  if (!$priority) {
	       $priority = 500;
	  }
	  if (is_numeric($priority) && $priority >= 0 && $priority <= 1000) {
	       $updates['priority'] = $priority;
	       $updates_fmt[] = "%d";
	  } else {
	       sardin_notify(
		    "Catégorie '$name' : entrer une priorité entre 0 et 1000",
		    'error');
	       continue;
	  }

	  $updated = $wpdb->update(
	       $categories_table,
	       $updates,
	       array('category_id' => $category_id),
	       $updates_fmt,
	       array("%d"));
	  if ($updated === false) {
	       sardin_notify(
		    "Catégorie '$name' : échec de la mise-à-jour", 'error');
	       return;
	  } elseif ($updated == 1) {
	       sardin_notify(
		    "Catégorie '$name' mise-à-jour");
	       $modifs += 1;
	  } elseif ($updated > 1) {
	       sardin_notify(
		    "Catégorie '$name' : pb, base de données corrompue ?",
		    'error');
	       return;
	  }
     }

     // Y a-t-il une nouvelle catégorie ?
     global $sardin_new_category;
     $sardin_new_category = @$_POST['sardin_new_category'];
     $sardin_new_category['name'] = @sanitize_text_field(
	  $sardin_new_category['name']);
     if ($sardin_new_category['name']) {
	  $query = $wpdb->prepare(
	       "SELECT COUNT(*) FROM $categories_table
                 WHERE name LIKE %s",
	       $sardin_new_category['name']);
	  $count = $wpdb->get_var($query);
	  if ($count > 0) {
	       sardin_notify("Ce nom de catégorie est déjà pris",
					 'error');
	       return;
	  }
	  $sardin_new_category['code'] = sanitize_text_field(
	       $sardin_new_category['code']);
	  $query = $wpdb->prepare(
	       "SELECT COUNT(*) FROM $categories_table
                 WHERE code LIKE %s",
	       $sardin_new_category['code']);
	  $count = $wpdb->get_var($query);
	  if ($count > 0) {
	       sardin_notify("Ce code de catégorie est déjà pris",
					 'error');
	       return;
	  }
	  $sardin_new_category['priority'] = sanitize_text_field(
	       $sardin_new_category['priority']);
	  if (!$sardin_new_category['priority']) {
	       $sardin_new_category['priority'] = 500;
	  }
	  if (!(is_numeric($sardin_new_category['priority'])
		&& $sardin_new_category['priority'] >= 0
		&& $sardin_new_category['priority'] <= 1000)) {
	       sardin_notify(
		    "Entrer une priorité entre 0 et 1000", 'error');
	       return;
	  }
	  $sardin_new_category['priority'] = intval(
	       $sardin_new_category['priority']);

	  $count = $wpdb->insert(
	       $categories_table,
	       array('name' => $sardin_new_category['name'],
		     'code' => $sardin_new_category['code'],
		     'priority' => $sardin_new_category['priority']),
	       array("%s", "%s", "%d"));
	  if ($count != 1) {
	       sardin_notify(
		    "Échec de la création d'une nouvelle catégorie", 'error');
	       return;
	  } else {
	       sardin_notify("Nouvelle catégorie créée");
	       $modifs += 1;
	       $sardin_new_category = null;
	  }
     }
     return true;
}

//// Fonctions utilitaires

// Messages aux utilisateurs, utilisable pendant la validation mais plus
// pendant l'affichage.
function sardin_notify($msg, $type='info') {
     global $sardin_tournaments_messages;
     if (count($sardin_tournaments_messages) == 0)
	  add_action('admin_notices', 'sardin_notify_action');

     array_push($sardin_tournaments_messages, array('type' => $type,
						    'content' => $msg));
}

function sardin_notify_action() {
     global $sardin_tournaments_messages;
     foreach ($sardin_tournaments_messages as $msg)
	  print "
<div class=\"notice notice-{$msg['type']} is-dismissible\">
  <p>{$msg['content']}</p>
</div>";
}

// Récupération du tournoi depuis le nom de la page affichée
function sardin_get_tournament_from_page() {
     $screen = get_current_screen();
     $matches = array();
     $page_pattern = "^[-_a-zA-Z0-9]+_page_".
		   str_replace('%d', '([0-9]+)',
			       SARDIN_CONFIG['tournament']['admin_slug']) ."\$";
     if (preg_match("/$page_pattern/", $screen->id, $matches)) {
	  $tournament_id = intval($matches[1]);
	  return sardin_get_tournament_by_id($tournament_id);
     }
     return;
}
