jQuery(document).ready(function() {
    jQuery('.sardin_checkbox_category').change(function() {
	var catcode = this.getAttribute('data-sardin-catcode');
	var disabled = !this.checked;
	jQuery("input.sardin_short_input[data-sardin-catcode='"+catcode+"']")
	    .prop('disabled', disabled);
    });

    jQuery('.sardin_checkbox_delete').change(function() {
	if (this.checked) {
	    var msg = "Attention, les données seront définitivement supprimées";
	    if (!confirm(msg)) {
		this.checked = false;
	    }
	}
    });
});

