<?php
if (!defined('SARDIN_CONFIG')) exit;

// Le shortcode sardin_tarifs
function sardin_tarifs_table($tournament, $caption) {
     $categories = sardin_get_categories($tournament, true);
     $lodgings = sardin_get_lodgings($tournament, $categories);
     $options = sardin_get_options($tournament, $categories);

     include(plugin_dir_path(__FILE__) . 'tarifs_table.html');
}
