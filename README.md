Sardin : extension WordPress de gestion de tournois de go
=========================================================

# Résumé

Cette extension permet de :

* créer des événements (tournois de go, stage, autres...)
* publier un formulaire d'inscription
* gérer la liste des inscrits

Cette extension ne permet pas de :

* payer en ligne

# Installation

**Télécharger l'archive**

Bouton **download** sur la [page du
projet](https://framagit.org/lregnier/sardin), choisir le format préféré
d'archive, télécharger, désarchiver ce qui va créer un dossier `sardin-master/`
contenant (les fichiers de) l'extension.

**Ou cloner le dépot**

La commande `git clone https://framagit.org/lregnier/sardin.git` va créer un
dossier `sardin/` contenant l'extension. Pour les mises à jour subséquentes
s'il y en a il faudra se rendre dans ce dossier et exécuter la commande `git
pull` (plus d'info sur l'outil `git` sur la [page
wikipedia](https://fr.wikipedia.org/wiki/Git) ou directement le [site du projet
git](https://git-scm.com/))

**Téléverser dans l'installation wordpress**

Utiliser son client `ftp` préféré (pas besoin de client ftp si WordPress est
installé sur la même machine) et téléverser le dossier `sardin-master/` (ou
`sardin/`) dans le dossier `wp-content/plugins/` de l'installation WordPress (à
l'exception du sous-dossier `.git/`) ; éventuellement renommer le dossier
`wp-content/plugins/sardin-master` en `wp-content/plugins/sardin`.

**Activer l'extension**

Se connecter sur l'interface d'admin de WordPress, ouvrir la page des
extensions, activer l'extension `Sardin` ; l'activation crée plusieurs tables
dans la base de donnée de WordPress, celles-ci seront conservées sans perte de
données si l'extension est désactivée, mais elles seront perdues si l'extension
est supprimée.

# Administration

Une fois l'extension activée, l'administrateur WordPress dispose d'une entrée
"Tournois" dans le menu d'administration ; celle-ci contient 3 fonctions:

- créer un nouveau tournoi
- gérer les catégories de tarif
- gérer les tournois déjà créés

## Création d'un tournoi

Un tournoi est défini par :

- son nom
- ses dates de début et de fin
- un email de contact
- au moins une formule d'hébergement
- des options

Optionnellement on peut ajouter un nombre maximum de participants, une date de
fin d'inscriptions.

Les formules d'hébergement ont pour paramètres : le type de formule (pension
complète, demi-pension, camping...), la durée et le tarif. Les formules
d'hébergement sont affichées dans l'ordre de durée décroissante.

Les options sont toutes les activités, payantes ou pas, proposées pendant le
tournoi, pour lesquelles les organisateurs souhaitent connaître en avance les
participants, par exemple le tournoi principal, un repas spécial du samedi
soir, un tournoi annexe (blitz, coinche, pétanque, ...).

Les options ont également une priorité, un entier compris entre 1 et 1000 qui
détermine l'ordre dans lequel elles seront affichées, de la plus grande à la
plus petite priorité.

Chaque formule d'hébergement et chaque option dispose d'un tarif (possiblement
égal à 0) par catégorie de tarif définie et activée pour le tournoi. Lorsqu'un
participant s'inscrit il déclare sa catégorie de tarif parmi celles qui lui
sont proposées, qui sont les catégories définies et actives pour le tournoi, ce
qui détermine le prix qu'il ou elle aura à payer.

Enfin on peut déclarer un certain nombre d'organisateurs qui seront habilités à
modifier les paramètres du tournoi et gérer les inscriptions ; les
organisateurs sont choisis parmi : les administrateurs du site qui sont
automatiquement organisateur de tous les tournois, les éditeurs et auteurs qui
doivent être déclarés explicitement par un autre organisateur du tournoi.

Une fois défini le tournoi, ses formules d'hébergements, ses options, ses
catégories de tarifs, on peut le déclarer 'publié' ; à partir de ce moment le
formulaire de définition du tournoi n'est plus accessible, remplacé par la page
de gestion des inscrits (mais on peut à tout moment "dépublier" le tournoi pour
le modifier ce qui n'est pas recommandé dès lors qu'il y a des inscrits).

## Catégories de tarif

Une catégorie de tarif est définie par sa description et son code ; il y a une
catégorie 'tarif normal' de code 'N', créée par défaut ; lorsque l'on crée une
catégorie la description devrait indiquer à qui celle-ci s'adresse, par exemple
'tarif jeune (plus de 12 ans, moins de 20 ans)' de code J.

Comme les options, les catégories ont une priorité de 1 à 1000 déterminant leur
ordre d'affichage ; la catégorie 'tarif normal' a priorité 1001 de façon à être
toujours en premier.

Les catégories de tarifs sont communes à tous les tournois, mais chaque tournoi
peut définir celles qu'il utilise ; seules celles qui sont activées pour le
tournoi seront proposées aux participants.

# Publication du tournoi

L'extension définit trois
[*shortcodes*](https://fr.support.wordpress.com/shortcodes/) pour faciliter
l'affichage du tournoi sur le site :

* le *shortcode* `[sardin_tarifs tournament="<nom du tournoi>" verbose="<oui ou
  non>"]` pour afficher le tableau des tarifs;
* le *shortcode* `[sardin_inscription tournament="<nom du tournoi>"
  verbose="<oui ou non>"]` pour afficher le formulaire d'inscription ;
* le *shortcode* `[sardin_participants tournament="<nom du tournoi>"
  show_count="<oui ou non>"]` pour afficher la liste des participants (avec ou
  sans leur nombre) ;

Ces trois *shortcodes* peuvent être disposés sur toute page ou article du
site. Ils ont tous un argument obligatoire `tournament` dont la valeur est
le nom du tournoi auquel ils s'appliquent.

Pour faciliter la personnalisation le HTML généré par chacun de ces *shortcodes*
est entièrement contenu dans un élément (`div`, `form` ou `table`) de classe
CSS `sardin_shortcode`. De nombreux autres éléments HTML générés ont leur
propre classe CSS, toutes préfixées par `sardin_`.

## Le tableau des tarifs

En ligne : les formules d'hébergements et les options.

En colonne : les catégories de tarifs.

Le paramètre optionnel `verbose` est positionné à `oui` par défaut, ce qui
affiche un petit baratin explicatif avant le tableau. Pour supprimer ce
baratin, définir `verbose` à `non` (ou `no`, `false`, `nook`).

## Le formulaire d'inscription

Le formulaire d'inscription se crée sur n'importe quelle page publique du site
(article de blog, page, ...) au moyen du *shortcode* `[sardin_inscription]` ;
en plus du paramètre obligatoire `tournament` déjà mentionné, on peut aussi
donner un paramètre optionnel `verbose` dont la valeur peut-être "oui" (ou
"yes", "ok", "true") ou "non" (ou "no", "nook", "false"). Par défaut `verbose`
est positionné à "oui" ce qui affiche un baratin explicatif en début de
formulaire.

Le formulaire permet d'inscrire plusieurs personnes arrivant et repartant en
même temps : toutes ces personnes auront donc la même formule
d'hébergement. Chacune déclare sa catégorie de tarif ce qui détermine
le coût de la formule d'hébergement et des options qu'elle souhaite réserver.

On peut également indiquer une adresse mail, ce qui permet de se faire envoyer
un mail de confirmation d'inscription.

Le formulaire d'inscription dispose également d'un bouton permettant de
retrouver une inscription déjà faite auparavant en se faisant envoyer un mail,
à condition bien sûr que l'adresse email de l'inscription ait été renseignée.

## La liste des participants

Les participants sont listés dans l'ordre chronologique d'inscriptions. Le
*shortcode* dispose d'un second paramètre optionnel `show_count` qui si il est
positionné à 'oui' (ou 'yes', 'ok', 'true') affiche le nombre de participants
inscrits avant la liste.

La liste des participants est également accessible aux organisateurs depuis
l'interface d'administration du tournoi lorsque celui-ci est publié. Dans ce
cas chaque nom de participant est un lien pointant vers le formulaire
d'inscription.

Cette page contient également, hormis le bouton de dépublication du tournoi
déjà mentionné, un bouton pour exporter la liste d'inscriptions au format CSV,
permettant ainsi d'ouvrir celle-ci dans un tableur.