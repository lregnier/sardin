<?php
// Liste des inscrits ; accessible aux visiteurs du site via le shortcode
// sardin_participants, ou par les organisateurs via la page d'admin du
// tournoi ; dans ce cas chaque participant est un lien vers son formulaire
// d'inscription et on affiche également un bouton de dépublication du tournoi

if (!defined('SARDIN_CONFIG')) exit;

function sardin_participants_list($tournament, $show_count=true) {
     $participants = sardin_get_participants($tournament);
     include(plugin_dir_path(__FILE__) . 'participants_list.html');
}

function sardin_html_participant($participant, $tournament) {
     if ($tournament['published'] != 0 && is_admin()
	 && sardin_is_organisor($tournament)) {
	  // C'est ici qu'il est utile d'avoir sauvé l'url du formulaire
	  // d'inscription...
	  $group_url = add_query_arg('sardin_group_token',
				     $participant['credential'],
				     $tournament['inscription_url']);
	  return "<a href=\"$group_url\">{$participant['name']}</a>";
     } else {
	  return $participant['name'];
     }
}
