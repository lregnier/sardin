<?php
// Ce script est déclenché via le shortcode sardin_inscription

if (!defined('SARDIN_CONFIG')) exit;

// Point d'entrée. Initialisation des données à afficher, validation éventuelle
// et affichage ; le paramètre $verbose indique si on doit afficher le baratin
// introductif
function sardin_inscription_form($tournament, $verbose=false) {
     if (isset($_REQUEST['sardin_reminder'])) {
	  return sardin_reminder_form($tournament);
     }

     if (@$_REQUEST['sardin_group_token']) {
	  $group = sardin_get_group_by_credential(
	       sanitize_text_field($_REQUEST['sardin_group_token']),
	       $tournament);
	  if (!$group) {
	       sardin_not_found();
	       return;
	  }
     } else {
	  $group = array(
	       'group_id' => '',
	       'credential' => '',
	       'arrival' => $tournament['start'],
	       'lodging' => '',
	       'email' => '',
	       'phone' => '',
	       'comment' => '',
	       'participants' => array(),
	       'total' => 0,
	  );
     }
     $categories = sardin_get_categories($tournament, true);
     $group['categories'] = $categories;
     $group['lodgings'] = sardin_get_lodgings($tournament, $categories);
     $group['options'] = sardin_get_options($tournament, $categories);
     $group['dates'] = array();
     $group['cancels'] = array();
     $start = new DateTime($tournament['start']);
     $end = new DateTime($tournament['end']);
     while ($start < $end) {
	  $sql_date = $start->format('Y-m-d');
	  $group['dates'][$sql_date] = sardin_html_date($sql_date, true);
	  $start->add(new DateInterval('P1D'));
     }

     $errors = array();
     if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	  $validated = sardin_validate_inscription(
	       $tournament, $group, $errors);
     } else {
	  $validated = null;
     }

     // Ajout d'un participant
     if (!(array_key_exists('new', $group) && is_array($group['new']))) {
	  $group['new'] = array(
	       'name' => '',
	       'catcode' => 'N',
	       'options' => array(),
	  );
     }

     // L'url du formulaire, pour créer un nouveau groupe
     $group['url'] = add_query_arg('sardin_group_token',
				   $group['credential'],
				   $tournament['inscription_url']);
     $mail = null;
     $mail_report = '';
     if ($group['email'] && $group['credential']) {
	  $group['sendmail_url'] = add_query_arg('sardin_mail_confirmation',
						 'yes', $group['url']);
	  // Faut il envoyer un mail ?
	  if (($_SERVER['REQUEST_METHOD'] == 'GET' || $validated)
	      && isset($_GET['sardin_mail_confirmation'])) {
	       $mail = sardin_send_confirmation_mail($tournament, $group);
	       $mail_report = $mail
			    ? "Un mail de confimation a été envoyé à <code>{$group['email']}</code>"
			    :  "Envoi du mail de confirmation échoué";
	  }
     }

     $expired_msg = sardin_tournament_expired($tournament);
     $form_url = add_query_arg(array('sardin_mail_confirmation' => false,
				     'sardin_group_token' => false));

     include(plugin_dir_path(__FILE__) . 'inscription_form.html');
}

// Inialisation du groupe
function sardin_get_group_by_credential($credential, $tournament) {
     global $wpdb;

     $groups_table = SARDIN_CONFIG['tables']['groups'];
     $items_table = SARDIN_CONFIG['tables']['items'];
     $participants_table = SARDIN_CONFIG['tables']['participants'];
     $categories_table = SARDIN_CONFIG['tables']['categories'];
     $registrations_table = SARDIN_CONFIG['tables']['registrations'];
     $tarifs_table = SARDIN_CONFIG['tables']['tarifs'];

     $query = $wpdb->prepare(
	  "SELECT $groups_table.*
             FROM $groups_table
             JOIN $items_table ON (item_id=lodging)
           WHERE credential = %s
             AND type = 'lodging'",
	  $credential);
     $group = $wpdb->get_row($query, ARRAY_A);
     if (!$group) {
	  return null;
     } elseif ($tournament
	      && $group['tournament_id'] != $tournament['tournament_id']) {
	  return null;
     }

     $query = $wpdb->prepare(
	  "SELECT $participants_table.participant_id,
                  $participants_table.name AS p_name,
                  $categories_table.name AS c_name,
                  $categories_table.code AS catcode,
                  $items_table.item_id,
                  $items_table.name AS i_name,
                  $items_table.type,
                  $tarifs_table.value
             FROM $participants_table
             JOIN $registrations_table USING(participant_id)
             JOIN $categories_table USING(category_id)
             JOIN $items_table USING(item_id)
             JOIN $tarifs_table USING(item_id)
            WHERE $tarifs_table.category_id = $participants_table.category_id
              AND group_id = %d
         ORDER BY $registrations_table.reg_date ASC",
	  $group['group_id']);
     $registrations = $wpdb->get_results($query, ARRAY_A);

     $group['total'] = 0.0;
     $ppants = array();
     foreach ($registrations as $reg) {
	  $p_id = $reg['participant_id'];
	  if (!isset($ppants[$p_id])) {
	       $ppants[$p_id] = array(
		    'name' => $reg['p_name'],
		    'catcode' => $reg['catcode'],
		    'options' => array(),
		    'total' => 0);
	  }
	  if ($reg['type'] == 'option') {
	       $ppants[$p_id]['options'][] = $reg['item_id'];
	  }
	  $ppants[$p_id]['total'] += $reg['value'];
	  $group['total'] += $reg['value'];
     }
     $group['participants'] = $ppants;
     return $group;
}

// Validation et sauvegarde ; retourne un message non vide en cas de succès, et
// un tableau indexé par les champs erronés des erreurs trouvées
function sardin_validate_inscription($tournament, &$group, &$errors) {
     global $wpdb;

     if ($group['credential'] != $_POST['sardin_group_token']) {
	  $errors['global_msg'] = "Invalid credential";
	  return false;
     }

     // On initialise ici le msg d'erreur global pour pouvoir faire des return
     // rapides, il sera détruit à la fin de la validation si il n'y a pas
     // d'erreurs
     $errors['global_msg'] = "Merci de corriger les erreurs signalées ci-dessous avant d'enregistrer les données de ce groupe";

     $arrival = sanitize_text_field($_POST['sardin_arrival']);
     if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}/', $arrival)
	 || $arrival < $tournament['start'] || $arrival > $tournament['end']) {
	  $errors['sardin_arrival'] = "Sélectionner une date d'arrivée valide";
	  return false;
     }
     $group['arrival'] = $arrival;

     $lodging_id = intval(sanitize_text_field($_POST['sardin_lodging']));
     if (!array_key_exists($lodging_id, $group['lodgings'])) {
	  $errors['sardin_lodging'] = "Sélectionner une formule d'hébergement valide";
	  return false;
     }
     $lodging = $group['lodgings'][$lodging_id];
     $departure = new DateTime($arrival);
     $departure->add(new DateInterval("P{$lodging['duration']}D"));
     $end = new DateTime($tournament['end']);
     if ($departure > $end) {
	  $errors['sardin_arrival'] = "Date trop tardive pour la durée du séjour";
	  $errors['sardin_lodging'] = "Séjour trop long pour cette date d'arrivée";
     }
     $group['old_lodging'] = $group['lodging'];
     $group['lodging'] = $lodging_id;

     $group['email'] = sanitize_email($_POST['sardin_email']);
     if ($group['email'] != $_POST['sardin_email']) {
	  $errors['sardin_email'] = "L'adresse mail proposée est invalide";
     } elseif (strlen($group['email']) == 0) {
	  $errors['sardin_email'] = "Entrer une adresse mail ; confidentielle mais vous permettra de recevoir le message de confirmation";
     } elseif (strlen($group['email']) > 64) {
	  $errors['sardin_email'] = "L'adresse email doit avoir moins de 64 caractères";
	  $group['email'] = substr($group['email'], 0, 64);
     }

     $group['phone'] = sanitize_text_field($_POST['sardin_phone']);
     if ($group['phone']) {
	  if (!preg_match('/^([+]?[0-9 ]+)([ ]*,[ ]*([+]?[0-9 ]+))*$/',
			  $group['phone'])) {
	       $errors['sardin_phone'] = "Entrer des numéros de téléphones séparés par des virgules";
	  } elseif (strlen($group['phone']) > 64) {
	       $errors['sardin_phone'] = "Les numéros de téléphone doivent totaliser moins de 64 caractères";
	       $group['phone'] = substr($group['phone'], 0, 64);
	  }
     }

     $group['comment'] = sanitize_textarea_field(
	  stripslashes($_POST['sardin_comment']));
     if (strlen($group['comment']) > 512) {
	  $errors['sardin_comment'] = "Les commentaires doivent tenir dans moins de 512 caractères";
	  $group['comment'] = substr($group['comment'], 0, 512);
     }

     $input_names = isset($_POST['sardin_participant_name'])
		  ? $_POST['sardin_participant_name'] : array();
     $input_categories = isset($_POST['sardin_participant_cat'])
		       ? $_POST['sardin_participant_cat'] : array();
     $input_cancels = isset($_POST['sardin_cancel'])
		    ? $_POST['sardin_cancel'] : array();
     $participants = array();
     $group['cancels'] = array();
     $total = 0;
     foreach ($group['participants'] as $p_id => $ppant) {
	  if (isset($input_cancels[$p_id])) {
	       $group['cancels'][$p_id] = $ppant;
	       continue;
	  }
	  $subtotal = 0;
	  $ppant['name'] = sanitize_text_field(stripslashes(
	       $input_names[$p_id]));

	  if (!$ppant['name']) {
	       $errors["sardin_participant_name[$p_id]"] = "Entrer un nom";
	  } elseif (strlen($ppant['name']) > 128) {
	       $errors["sardin_participant_name[$p_id]"] = "Le nom doit avoir moins de 128 caractères";
	       $ppant['name'] = substr($ppant['name'], 0, 128);
	  }

	  $catcode = sanitize_text_field($input_categories[$p_id]);
	  if (!isset($group['categories'][$catcode])) {
	       $errors["sardin_participant_cat[$p_id]"] = "Sélectionner un tarif valide";
	       return false;
	  }
	  $ppant['catcode'] = $catcode;
	  $subtotal += $lodging['tarifs'][$catcode]['value'];

	  $options = array();
	  $input_options = isset($_POST["sardin_participant_options:$p_id"])
			 ? $_POST["sardin_participant_options:$p_id"] : array();
	  foreach ($input_options as $option_id) {
	       if (!array_key_exists($option_id, $group['options'])) {
		    $errors["sardin_participant_options:$p_id"] = "Sélectionner des options valides";
		    return false;
	       }
	       $options[] = $option_id;
	  }
	  $old_options = $ppant['options'];
	  $ppant['options'] = $options;
	  $ppant['new_options'] = array_diff($options, $old_options);
	  $ppant['deleted_options'] = array_diff($old_options, $options);
	  foreach ($group['options'] as $option) {
	       if (in_array($option['item_id'], $ppant['options'])) {
		    $subtotal += $option['tarifs'][$catcode]['value'];
	       }
	  }
	  $ppant['total'] = $subtotal;
	  $participants[$p_id] = $ppant;
	  $total += $subtotal;
     }
     $group['participants'] = $participants;

     $group['new'] = null;
     if (@$_POST['sardin_new_participant_name']) {
	  if (sardin_tournament_expired($tournament)) {
	       $errors['global_msg'] = "Désolé, les inscriptions sont closes";
	       return false;
	  }

	  $subtotal = 0;
	  $ppant = array();
	  $name = sanitize_text_field(stripslashes(
	       $_POST['sardin_new_participant_name']));
	  if (!$name) {
	       $errors["sardin_new_participant_name"] = "Entrer un nom valide";
	       return false;
	  } elseif (strlen($name) > 128) {
	       $errors["sardin_new_participant_name"] = "Le nom doit avoir moins de 128 caractères";
	       $name = substr($name, 0, 128);
	  } elseif ($wpdb->get_var($wpdb->prepare(
	       "SELECT COUNT(*)
                  FROM ". SARDIN_CONFIG['tables']['participants'] ."
                  JOIN ". SARDIN_CONFIG['tables']['registrations'] ."
                 USING (participant_id)
                  JOIN ". SARDIN_CONFIG['tables']['groups'] ."
                 USING (group_id)
                 WHERE name LIKE %s
                   AND tournament_id = %d",
	       $name, $tournament['tournament_id'])) > 0) {
	       $errors["sardin_new_participant_name"] = "Cette personne est déjà inscrite dans ce tournoi";
	  }
	  $ppant['name'] = $name;

	  $catcode = sanitize_text_field($_POST['sardin_new_participant_cat']);
	  if (!isset($group['categories'][$catcode])) {
	       $errors["sardin_new_participant_cat"] = "Sélectionner un tarif valide";
	       return false;
	  }
	  $ppant['catcode'] = $catcode;
	  $subtotal += $lodging['tarifs'][$catcode]['value'];

	  $options = array();
	  if (is_array(@$_POST['sardin_new_participant_options'])) {
	       foreach ($_POST['sardin_new_participant_options']
			as $option_id) {
		    if (!array_key_exists($option_id, $group['options'])) {
			 $errors["sardin_new_participant"] = "Sélectionner des options valides";
			 return false;
		    }
		    $options[] = $option_id;
		    $subtotal += $group['options'][$option_id]['tarifs'][$catcode]['value'];
	       }
	  }
	  $ppant['options'] = $options;
	  $ppant['total'] = $subtotal;
	  $total += $subtotal;
	  $group['new'] = $ppant;
     }

     $group['total'] = $total;

     if (count($errors) > 1) {
	  // On est en train de supprimer ce groupe ? -> pas la peine
	  // d'afficher des erreurs
	  if ($group['total'] <= 0) {
	       $errors = array();
	       unset($errors['global_msg']);
	  } else
	       return false;
     } else {
	  // Pas d'erreur ? Si : le msg global que l'on avait initialisé
	  unset($errors['global_msg']);
     }

     // Et maintenant, sauvegarde
     $date = date('Y-m-d H:i:s');
     if ($group['credential']) {
	  $result = $wpdb->update(
	       SARDIN_CONFIG['tables']['groups'],
	       array('email' => $group['email'],
		     'phone' => $group['phone'],
		     'arrival' => $group['arrival'],
		     'lodging' => $group['lodging'],
		     'comment' => $group['comment']),
	       array('group_id' => $group['group_id']),
	       array("%s", "%s", "%s", "%d", "%s"),
	       array("%d"));
	  if ($result === false) {
	       $errors['global_msg'] = "Mise-à-jour du groupe échouée";
	       return false;
	  }

	  foreach ($group['cancels'] as $p_id => $ppant) {
	       $result = $wpdb->delete(
		    SARDIN_CONFIG['tables']['registrations'],
		    array('participant_id' => $p_id,
			  'group_id' => $group['group_id']),
		    array("%d", "%d"));
	       if ($result === false) {
		    $errors['global_msg'] = "Suppression des options de {$ppant['name']} échouée";
		    return false;
	       }
	       $result = $wpdb->delete(
		    SARDIN_CONFIG['tables']['participants'],
		    array('participant_id' => $p_id),
		    array("%d"));
	       if ($result === false) {
		    $errors['global_msg'] = "Suppression de {$ppant['name']} échouée";
		    return false;
	       }
	  }
	  if ($group['old_lodging'] != $group['lodging']) {
	       $result = $wpdb->update(
		    SARDIN_CONFIG['tables']['registrations'],
		    array('item_id' => $group['lodging'],
			  'mod_date' => $date),
		    array('group_id' => $group['group_id'],
			  'item_id' => $group['old_lodging']),
		    array("%d", "%s"),
		    array("%d", "%d"));
	       if ($result === false) {
		    $errors['global_msg'] = "Mise-à-jour de la formule d'hébergement échouée";
		    return false;
	       }
	  }
	  foreach ($group['participants'] as $p_id => $ppant) {
	       $result = $wpdb->update(
		    SARDIN_CONFIG['tables']['participants'],
		    array('name' => $ppant['name'],
			  'category_id' => $group['categories'][$ppant['catcode']]['category_id']),
		    array('participant_id' => $p_id),
		    array("%s", "%d"),
		    array("%d"));
	       if ($result === false) {
		    $errors['global_msg'] = "Mise-à-jour de {$ppant['name']} échouée";
		    return false;
	       }

	       foreach ($ppant['new_options'] as $option_id) {
		    $result = $wpdb->insert(
			 SARDIN_CONFIG['tables']['registrations'],
			 array('participant_id' => $p_id,
			       'group_id' => $group['group_id'],
			       'item_id' => $option_id,
			       'reg_date' => $date,
			       'mod_date' => $date),
			 array("%d", "%d", "%d", "%s", "%s"));
		    if ($result === false) {
			 $errors['global_msg'] = "Mise-à-jour des nouvelles options de {$ppant['name']} échouée";
			 return false;
		    }
	       }
	       foreach ($ppant['deleted_options'] as $option_id) {
		    $result = $wpdb->delete(
			 SARDIN_CONFIG['tables']['registrations'],
			 array('participant_id' => $p_id,
			       'group_id' => $group['group_id'],
			       'item_id' => $option_id),
			 array("%d", "%d", "%d"));
		    if ($result === false) {
			 $errors['global_msg'] = "Suppression des options de {$ppant['name']} échouée";
			 return false;
		    }
	       }
	  }
     } else {
	  if (!$group['new']) {
	       $errors['global_msg'] = "Inscrivez un participant dans ce groupe";
	       return false;
	  }
	  $credential = sardin_make_credential();
	  $result = $wpdb->insert(
	       SARDIN_CONFIG['tables']['groups'],
	       array('tournament_id' => $tournament['tournament_id'],
		     'email' => $group['email'],
		     'phone' => $group['phone'],
		     'arrival' => $group['arrival'],
		     'lodging' => $group['lodging'],
		     'comment' => $group['comment'],
		     'credential' => $credential),
	       array("%d", "%s", "%s", "%s", "%d", "%s", "%s"));
	  if ($result === false) {
	       $errors['global_msg'] = "Création du groupe échouée";
	       return false;
	  } else {
	       // On (re)sauve l'url du formulaire d'inscription
	       $inscription_url = home_url(add_query_arg(array()));
	       $wpdb->update(
		    SARDIN_CONFIG['tables']['tournaments'],
		    array('inscription_url' => $inscription_url),
		    array('tournament_id' => $tournament['tournament_id']),
		    array("%s"),
		    array("%d"));
	  }
	  $group['group_id'] = $wpdb->insert_id;
	  $group['credential'] = $credential;
	  $tournament['inscription_url'] = $inscription_url;
     }

     // Et pour finir on ajoute un éventuel participant
     if ($group['new']) {
	  $new = $group['new'];
	  $result = $wpdb->insert(
	       SARDIN_CONFIG['tables']['participants'],
	       array('name' => $new['name'],
		     'category_id' => $group['categories'][$new['catcode']]['category_id']),
	       array("%s", "%d"));
	  if ($result === false) {
	       $errors['global_msg'] = "Création de {$new['name']} échouée";
	       // TODO dans le cas d'une création de groupe on va se retrouver
	       // avec un groupe vide
	       return false;
	  }
	  $p_id = $wpdb->insert_id;
	  $result = $wpdb->insert(
	       SARDIN_CONFIG['tables']['registrations'],
	       array('participant_id' => $p_id,
		     'group_id' => $group['group_id'],
		     'item_id' => $group['lodging'],
		     'reg_date' => $date,
		     'mod_date' => $date),
	       array("%d", "%d", "%d", "%s", "%s"));
	  if ($result === false) {
	       $errors['global_msg'] = "Inscription de {$new['name']} échouée";
	        // TODO dans le cas d'une création de groupe on va se retrouver
	        // avec un groupe sans participant inscrit...
	       return false;
	  }
	  $group['new'] = null;
	  $group['participants'][$p_id] = $new;

	  foreach($new['options'] as $option_id) {
	       $result = $wpdb->insert(
		    SARDIN_CONFIG['tables']['registrations'],
		    array('participant_id' => $p_id,
			  'group_id' => $group['group_id'],
			  'item_id' => $option_id,
			  'reg_date' => $date,
			  'mod_date' => $date),
		    array("%d", "%d", "%d", "%s", "%s"));
	       if ($result === false) {
		    $errors['global_msg'] = "Ajout des options de {$ppant['name']} échouée";
		    return false;
	       }
	  }
     }

     if (count($group['participants']) == 0) {
	  $wpdb->delete(
	       SARDIN_CONFIG['tables']['groups'],
	       array('group_id' => $group['group_id']),
	       array("%d"));
	  $group['group_id'] = '';
	  $group['credential'] = '';
	  $errors['global_msg'] = "Plus de participant&nbsp;: ce groupe est supprimé";
	  return false;

     }
     // Ouf !
     return true;
}

function sardin_make_credential() {
     global $wpdb;

     $credential = bin2hex(random_bytes(16));
     while ($wpdb->get_var(
	  "SELECT COUNT(*) FROM ". SARDIN_CONFIG['tables']['groups'] ."
            WHERE credential = '$credential'") > 0) {
	  $credential = bin2hex(random_bytes(8));
     }
     return $credential;
}

// Retourne false si on est organisateur ou si le tournoi n'est pas expiré, la
// raison de l'expiration sinon
function sardin_tournament_expired($tournament) {
     if (sardin_is_organisor($tournament))
	  return false;

     if ($tournament['max_participants']
	 && count(sardin_get_participants($tournament)) >= $tournament['max_participants']) {
	  return "Le nombre limite d'inscriptions est atteint, pour en ajouter merci de contacter les organisateurs.";
     }
     if ($tournament['reg_end'] && $tournament['reg_end'] < date('Y-m-d')) {
	  return "La date limite d'inscription est passée, pour ajouter des inscriptions merci de contacter les organisateurs";
     }
     return false;
}


function sardin_reminder_form($tournament) {
     $message = '';
     $mails = null;
     if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	  if ($mails = sardin_validate_reminder($tournament, $message)) {
	       include(plugin_dir_path(__FILE__) . 'reminder_mailsent.html');
	       return;
	  }
     }
     include(plugin_dir_path(__FILE__) . 'reminder_form.html');
}

function sardin_validate_reminder($tournament, &$message) {
     global $wpdb;

     $input_email = @$_POST['sardin_reminder_email'];
     if (!$input_email) {
	  $message = "Entrer une adresse mail";
	  return false;
     }
     $reminder_email = sanitize_email($input_email);
     if ($reminder_email != $input_email) {
	  $message = "Entrer une adresse mail valide";
	  return false;
     }

     $query = $wpdb->prepare(
	  "SELECT credential FROM ". SARDIN_CONFIG['tables']['groups'] ."
            WHERE email = %s",
	  $reminder_email);
     $credentials = $wpdb->get_col($query);
     if (count($credentials) == 0) {
	  $message = "Il n'y a pas d'inscription correspondant à cette adresse";
	  return false;
     }

     $categories = sardin_get_categories($tournament, true);
     $mails = array();
     foreach ($credentials as $credential) {
	  $group = sardin_get_group_by_credential($credential, $tournament);
	  $group['categories'] = $categories;
	  $group['lodgings'] = sardin_get_lodgings($tournament, $categories);
	  $group['options'] = sardin_get_options($tournament, $categories);
	  $mail = sardin_send_confirmation_mail($tournament, $group);
	  if (!$mail) {
	       $message = "Échec de l'envoi du mail";
	       return false;
	  }
	  $mails[] = $mail;
     }
     return $mails;
}

function sardin_send_confirmation_mail($tournament, $group) {
     $group_url = add_query_arg('sardin_group_token', $group['credential'],
				$tournament['inscription_url']);
     $arrival_date = lcfirst(sardin_html_date($group['arrival'], true));
     $duration = $group['lodgings'][$group['lodging']]['duration'];
     $duration = $duration > 1 ? "$duration nuitées" : "$duration nuitée";
     $mail = array('to' => $group['email'],
		   'subject' => "{$tournament['name']} - Inscription",
		   'body' => null,
		   'headers' => array("From: {$tournament['contact_mail']}",));
     if (count($group['participants']) == 1) {
	  $mail['body'] = "

Voici un récapitulatif(*) de votre inscription à : {$tournament['name']}.

Vous pouvez la modifier, en ajouter ou l'annuler à l'adresse suivante :";
     } else {
	  $mail['body'] = "

Voici un récapitulatif(*) de vos inscriptions à : {$tournament['name']}.

Vous pouvez les modifier, en ajouter ou annuler à l'adresse suivante :";
     }
     $mail['body'] .= "

     $group_url

Date d'arrivée :  $arrival_date
Durée du séjour : $duration

Participants :";
     foreach ($group['participants'] as $ppant) {
	  $mail['body'] .= "

- {$ppant['name']}";
	  if (count($ppant['options']) > 0) {
	       $options = array();
	       foreach ($ppant['options'] as $option_id) {
		    $options[] = $group['options'][$option_id]['name'];
	       }
	       $options = implode(', ', $options);
	       $mail['body'] .= "
  Options : $options.";
	       }
	  $mail['body'] .= "
  Sous-total participant (tarif {$group['categories'][$ppant['catcode']]['name']}) : {$ppant['total']} €";
     }
     $mail['body'] .= "

Total du groupe : {$group['total']} €

Important : merci de nous prévenir en cas d'annulation, soit en vous
désinscrivant sur la page indiquée ci-dessus, soit en nous écrivant (répondre
au présent message).

L'équipe d'organisation vous remercie.
--------------------------------------

(*) Si vous n'avez pas sollicité l'envoi de ce message vous pouvez nous le
signaler en répondant au présent message, ou simplement l'ignorer.
";

     if (@SARDIN_DEBUG || wp_mail($mail['to'], $mail['subject'],
				  $mail['body'], $mail['headers'])) {
	  return $mail;
     } else {
	  return false;
     }
}

// Utilitaires pour l'affichage
function sardin_error_to_html($errors, $field) {
     if (array_key_exists($field, $errors)) {
	  return "<p>$errors[$field]</p>";
     } else {
	  return "";
     }
}

function sardin_error_html_class($errors, $field) {
     if (array_key_exists($field, $errors)) {
	  return "class=\"sardin_error\"";
     } else {
	  return "";
     }
}
