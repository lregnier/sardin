<?php
if (!defined('SARDIN_CONFIG')) exit;

function sardin_update_tables() {
     global $wpdb;

     $tournaments_table = SARDIN_CONFIG['tables']['tournaments'];
     $items_table = SARDIN_CONFIG['tables']['items'];
     $categories_table = SARDIN_CONFIG['tables']['categories'];
     $tmnt_categories_table = SARDIN_CONFIG['tables']['tmnt_categories'];
     $tarifs_table = SARDIN_CONFIG['tables']['tarifs'];
     $participants_table = SARDIN_CONFIG['tables']['participants'];
     $groups_table = SARDIN_CONFIG['tables']['groups'];
     $registrations_table = SARDIN_CONFIG['tables']['registrations'];

     require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

     $charset_collate = $wpdb->get_charset_collate();
     $sql_create_table = "CREATE TABLE IF NOT EXISTS";

     $create_tournaments_table = "$sql_create_table $tournaments_table (
  tournament_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(191) NOT NULL UNIQUE,
  contact_mail VARCHAR(128) NOT NULL,
  start DATE NOT NULL,
  end DATE NOT NULL,
  max_participants INT(11) NULL,
  reg_end DATE NULL,
  published TINYINT(1) NOT NULL DEFAULT 0,
  inscription_url VARCHAR(256) NOT NULL DEFAULT ''
) $charset_collate;";

     $create_items_table = "$sql_create_table $items_table (
  item_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  tournament_id INT(11) NOT NULL REFERENCES $tournaments_table(tournament_id),
  name VARCHAR (191) NOT NULL,
  type ENUM('lodging', 'option') NOT NULL DEFAULT 'lodging',
  duration SMALLINT(3) NOT NULL DEFAULT 30
) $charset_collate;";

     $create_categories_table = "$sql_create_table $categories_table (
  category_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(191) NOT NULL,
  code VARCHAR(16) NOT NULL,
  priority SMALLINT(3) NOT NULL DEFAULT 10
) $charset_collate;";

     $create_tmnt_categories_table = "
  $sql_create_table $tmnt_categories_table (
  category_id INT(11) NOT NULL REFERENCES $categories_table(category_id),
  tournament_id INT(11) NOT NULL REFERENCES $tournaments_table(tournament_id),
  enabled TINYINT(1) NOT NULL DEFAULT 1,
  UNIQUE (category_id, tournament_id)
) $charset_collate;";

     $create_tarifs_table =  "$sql_create_table $tarifs_table (
  tarif_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  item_id INT(11) NOT NULL REFERENCES $items_table(item_id),
  category_id INT(11) NOT NULL REFERENCES $categories_table(category_id),
  value DECIMAL(7, 2) NOT NULL
) $charset_collate;";

     $create_participants_table = "$sql_create_table $participants_table (
  participant_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(191) NOT NULL,
  category_id INT(11) NOT NULL REFERENCES $categories_table(category_id)
) $charset_collate;";

     $create_groups_table = "$sql_create_table $groups_table (
  group_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  tournament_id INT(11) NOT NULL REFERENCES $tournaments_table(tournament_id),
  email VARCHAR(128) NULL,
  phone VARCHAR(64) NULL,
  arrival DATE NOT NULL,
  lodging INT(11) NOT NULL REFERENCES $items_table(item_id),
  comment VARCHAR(512) NOT NULL DEFAULT '',
  credential CHAR(32) NOT NULL UNIQUE
) $charset_collate;";

     $create_registrations_table = "$sql_create_table $registrations_table (
  participant_id INT(11) NOT NULL REFERENCES $participants_table(participant_id),
  group_id INT(11) NOT NULL REFERENCES $groups_table(group_id),
  item_id INT(11) NOT NULL REFERENCES $items_table(item_id),
  reg_date DATETIME not NULL,
  mod_date DATETIME not NULL
) $charset_collate;";

     $wpdb->query($create_tournaments_table);
     $wpdb->query($create_items_table);
     $wpdb->query($create_categories_table);
     $wpdb->query($create_tmnt_categories_table);
     $wpdb->query($create_tarifs_table);
     $wpdb->query($create_participants_table);
     $wpdb->query($create_groups_table);
     $wpdb->query($create_registrations_table);

     // Il faut au moins une catégorie
     if (!$wpdb->get_row("SELECT * FROM $categories_table
                          WHERE category_id = 1"))
	  $wpdb->insert($categories_table,
			array('category_id' => 1,
			      'name' => 'normal',
			      'code' => 'N',
			      'priority' => 1001),
			array("%d", "%s", "%s"));
}

function sardin_html_date($sql_date, $verbose=false) {
     if (!$sql_date) return;
     $time = strtotime($sql_date);
     if ($verbose) {
	  return ucfirst(date_i18n("l j F Y", $time));
     } else {
	  return date("d/m/Y", $time);
     }
}

function sardin_sql_date($html_date) {
     if (!$html_date) return;
     list($day, $month, $year) = preg_split('<[/-]>', $html_date);
     return sprintf("%d-%02d-%02d", $year, $month, $day);
}

function sardin_drop_tables() {
     global $wpdb;

     $wpdb->show_errors();
     foreach (array_reverse(SARDIN_CONFIG['tables']) as $table)
	  $wpdb->query("DROP TABLE IF EXISTS $table");
}

function sardin_get_tournament($data) {
     if (is_int($data)) {
	  return sardin_get_tournament_by_id($data);
     } elseif (is_string($data)) {
	  return sardin_get_tournament_by_name($data);
     } else {
	  return;
     }
}

function sardin_get_tournament_by_id($tournament_id) {
     global $wpdb;

     $query = $wpdb->prepare(
	  "SELECT * FROM ". SARDIN_CONFIG['tables']['tournaments'] ."
            WHERE tournament_id = %d",
	  $tournament_id);
     $tournament = $wpdb->get_row($query, ARRAY_A);

     // Les champs texte sont stockés dans la base en mode protégé
     if ($tournament) {
	  $tournament['name'] = stripslashes($tournament['name']);
	  return $tournament;
     }
     return null;
}

// Comme son nom l'indique
function sardin_get_tournament_by_name($name) {
     global $wpdb;

     $query = $wpdb->prepare(
	  "SELECT * FROM ". SARDIN_CONFIG['tables']['tournaments'] ."
            WHERE name like %s",
	  $name);
     $tournament = $wpdb->get_row($query, ARRAY_A);

     // Les champs texte sont stockés dans la base en mode protégé
     if ($tournament) {
	  $tournament['name'] = stripslashes($tournament['name']);
	  return $tournament;
     }
     return null;
}

// Récupération des catégories de tarifs, toutes ou seulement les actives selon
// le paramètre $enabled_only
function sardin_get_categories($tournament=null, $enabled_only=false) {
     global $wpdb;

     $categories = $wpdb->get_results(
	  "SELECT * FROM ". SARDIN_CONFIG['tables']['categories'] ."
           ORDER BY priority DESC",
	  ARRAY_A);

     if ($tournament) {
	  $enabled = $wpdb->get_col(
	       "SELECT category_id
                  FROM ". SARDIN_CONFIG['tables']['tmnt_categories'] ."
                 WHERE tournament_id = {$tournament['tournament_id']}
                   AND enabled <> 0");

	  foreach ($categories as &$category) {
	       $category['enabled'] = in_array($category['category_id'],
					       $enabled);
	  }
	  // Php de daube ! Mais pourquoi faut-il faire ça ?
	  unset($category);
     } else {
	  // Les catégories sont activées par tournoi
	  $enabled_only = false;
     }
     // Pour sardin_validate_lodgings() on a besoin d'indexer les catégories
     // par code
     $categories_by_code = array();
     foreach($categories as $category) {
	  if ($enabled_only && !$category['enabled']) continue;
	  $category['name'] = stripslashes($category['name']);
	  $categories_by_code[$category['code']] = $category;
     }
     return $categories_by_code;
}

// (Dés)activation des catégories de tarifs par tournoi
function sardin_enable_category($tournament, $category=null, $enabled=true) {
     global $wpdb;

     // Par défaut on active la catégorie normale
     if (!$category) {
	  $category = $wpdb->get_row(
	       "SELECT * FROM ". SARDIN_CONFIG['tables']['categories'] ."
                 WHERE code = 'N'", ARRAY_A);
     }

     if ($wpdb->get_var(
	  "SELECT COUNT(*) FROM ". SARDIN_CONFIG['tables']['tmnt_categories'] ."
            WHERE tournament_id = {$tournament['tournament_id']}
              AND category_id = {$category['category_id']}") == 0) {
	  if (!$enabled) return;
	  $wpdb->insert(SARDIN_CONFIG['tables']['tmnt_categories'],
			array('tournament_id' => $tournament['tournament_id'],
			      'category_id' => $category['category_id'],
			      'enabled' => $enabled),
			array("%d", "%d", "%d"));
     } else {
	  $wpdb->update(SARDIN_CONFIG['tables']['tmnt_categories'],
			array('enabled' => $enabled),
			array('tournament_id' => $tournament['tournament_id'],
			      'category_id' => $category['category_id']),
			array("%d"),
			array("%d", "%d") );
     }
}

// Récupération des formules d'hébergement et de leurs tarifs si le paramètre
// $categories est positionné
function sardin_get_lodgings($tournament, $categories=null) {
     global $wpdb;

     if (!$tournament)
	  return;

     $query = $wpdb->prepare(
	  "SELECT * FROM ". SARDIN_CONFIG['tables']['items'] ."
           WHERE tournament_id = %d
             AND type = 'lodging'
           ORDER BY duration DESC",
	  $tournament['tournament_id']);
     $lodgings = $wpdb->get_results($query, ARRAY_A);

     if (!$categories) {
	  return $logdgings;
     }

     $lodgings_by_id = array();
     foreach ($lodgings as &$lodging) {
	  $lodging['name'] = stripslashes($lodging['name']);
	  $lodging['tarifs'] = array();
	  $values = $wpdb->get_results(
		    "SELECT category_id, value
                       FROM ". SARDIN_CONFIG['tables']['tarifs'] ."
                      WHERE item_id = {$lodging['item_id']}",
		    OBJECT_K);
	  foreach($categories as $category) {
	       $lodging['tarifs'][$category['code']] = array(
		    'value' => @$values[$category['category_id']]->value,
		    'enabled' => $category['enabled'] == 0 ? false : true);
	  }
	  $lodgings_by_id[$lodging['item_id']] = $lodging;
     }
     return $lodgings_by_id;
}

// Idem mais pour les autres options
function sardin_get_options($tournament, $categories=null)  {
     global $wpdb;

     if (!$tournament)
	  return;

     $query = $wpdb->prepare(
	  "SELECT * FROM ". SARDIN_CONFIG['tables']['items'] ."
           WHERE tournament_id = %d
             AND type = 'option'
           ORDER BY duration DESC",
	  $tournament['tournament_id']);
     $options = $wpdb->get_results($query, ARRAY_A);

     if (!$categories) {
	  return $options;
     }

     $options_by_id = array();
     foreach ($options as $option) {
	  $option['tarifs'] = array();
	  $values = $wpdb->get_results(
		    "SELECT category_id, value
                       FROM ". SARDIN_CONFIG['tables']['tarifs'] ."
                      WHERE item_id = {$option['item_id']}",
		    OBJECT_K);
	  foreach($categories as $category) {
	       $option['tarifs'][$category['code']] = array(
		    'value' => @$values[$category['category_id']]->value,
		    'enabled' => $category['enabled'] == 0 ? false : true);
	  }
	  $options_by_id[$option['item_id']] = $option;
     }
     return $options_by_id;
}

// Suppression d'une formule d'hébergement/option
function sardin_delete_option($option) {
     global $wpdb;
     $wpdb->delete(SARDIN_CONFIG['tables']['tarifs'],
		   array('item_id' => $option['item_id']),
		   array("%d"));
     $wpdb->delete(SARDIN_CONFIG['tables']['items'],
		   array('item_id' => $option['item_id']),
		   array("%d"));
}

// Participants à un tournoi
function sardin_get_participants($tournament) {
     global $wpdb;

     $participants_table = SARDIN_CONFIG['tables']['participants'];
     $registrations_table = SARDIN_CONFIG['tables']['registrations'];
     $groups_table = SARDIN_CONFIG['tables']['groups'];
     $items_table = SARDIN_CONFIG['tables']['items'];
     $query = $wpdb->prepare(
	  "SELECT $participants_table.name, $groups_table.credential
             FROM $participants_table
             JOIN $registrations_table USING (participant_id)
             JOIN $items_table USING (item_id)
             JOIN $groups_table USING (group_id)
            WHERE type = 'lodging'
              AND $groups_table.tournament_id = %d
         ORDER BY reg_date ASC",
	  $tournament['tournament_id']);
     return $participants = $wpdb->get_results($query, ARRAY_A);
}

// Participants à un tournoi avec leurs options
function sardin_get_participants_with_options($tournament) {
     global $wpdb;

     $participants_table = SARDIN_CONFIG['tables']['participants'];
     $categories_table =SARDIN_CONFIG['tables']['categories'];
     $items_table = SARDIN_CONFIG['tables']['items'];
     $groups_table = SARDIN_CONFIG['tables']['groups'];
     $registrations_table = SARDIN_CONFIG['tables']['registrations'];
     $tarifs_table = SARDIN_CONFIG['tables']['tarifs'];
     $query = $wpdb->prepare(
	  "SELECT $participants_table.*, $groups_table.*,
                  $categories_table.code as catcode,
                  $categories_table.name as catname,
                  $registrations_table.reg_date, $registrations_table.mod_date,
                  $items_table.item_id,
                  $items_table.name as item_name,
                  $items_table.duration, $items_table.type,
                  $tarifs_table.value
             FROM $participants_table
             JOIN $categories_table USING(category_id)
             JOIN $registrations_table USING (participant_id)
             JOIN $groups_table USING (group_id)
             JOIN $items_table USING (item_id)
             JOIN $tarifs_table USING(item_id)
            WHERE $groups_table.tournament_id = %d
              AND $tarifs_table.category_id = $participants_table.category_id
         ORDER BY group_id, reg_date ASC",
	  $tournament['tournament_id']);
     $registrations = $wpdb->get_results($query, ARRAY_A);

     if ($registrations === false) {
	  return array();
     }

     $participants = array();
     foreach ($registrations as $reg) {
	  if (!array_key_exists($reg['participant_id'], $participants)) {
	       $participants[$reg['participant_id']] = array(
		    'name' => $reg['name'],
		    'reg_date' => '',
		    'mod_date' => '',
		    'catname' => $reg['catname'],
		    'catcode' => $reg['catcode'],
		    'lodging_id' => '',
		    'lodgin_name' => '',
		    'arrival' => '',
		    'duration' => '',
		    'lodging_fee' => '',
		    'group_id' => $reg['group_id'],
		    'email' => $reg['email'],
		    'phone' => $reg['phone'],
		    'comment' => $reg['comment'],
		    'options' => array());
	  }
	  $participant = &$participants[$reg['participant_id']];
	  if ($reg['type'] == 'lodging') {
	       $participant['reg_date'] = $reg['reg_date'];
	       $participant['mod_date'] = $reg['mod_date'];
	       $participant['lodging_id'] = $reg['item_id'];
	       $participant['lodging_name'] = $reg['item_name'];
	       $participant['arrival'] = $reg['arrival'];
	       $participant['duration'] = $reg['duration'];
	       $participant['lodging_fee'] = $reg['value'];
	  } elseif ($reg['type'] == 'option') {
	       $participant['options'][$reg['item_id']] = array(
		    'name' => $reg['item_name'],
		    'reg_date' => $reg['reg_date'],
		    'mod_date' => $reg['mod_date'],
		    'fee' => $reg['value']);
	  }
     }
     return $participants;
}

function sardin_has_participants($tournament) {
     global $wpdb;

     return $wpdb->get_var($wpdb->prepare(
	  "SELECT COUNT(*) FROM ". SARDIN_CONFIG['tables']['groups'] ."
            WHERE tournament_id = %d",
	  $tournament['tournament_id'])) > 0;
}

