<?php
if (!defined('SARDIN_CONFIG')) exit;

// Les utilisateurs pouvant être promus organisateur de tournoi
function sardin_authorized_members() {
     return get_users(
	  array('role__in' => SARDIN_CONFIG['roles']['manage_tournament']));
}

// Droit de créer des nouveaux tournois ou catégories de tarifs
function sardin_is_manager($user=false) {
     if (!$user) {
	  $user = wp_get_current_user();
     }
     return user_can($user, SARDIN_CONFIG['caps']['tournaments']);
}

// Organisateur d'un tournoi
function sardin_is_organisor($tournament, $user=false) {
     $tournament_id = is_numeric($tournament)
		    ? $tournament : $tournament['tournament_id'];
     $cap = sprintf(SARDIN_CONFIG['caps']['tournament'], $tournament_id);
     if (!$user) {
	  $user = wp_get_current_user();
     }
     return user_can($user, $cap);
}

// Permissions globales, pour l'activation du plugin ; chaque rôle 'manager'
// reçoit chaque permission de tournoi
function sardin_create_capabilities() {
     global $wpdb;
     $tournaments_ids = $wpdb->get_col(
	       "SELECT tournament_id
                  FROM ". SARDIN_CONFIG['tables']['tournaments']);

     foreach (SARDIN_CONFIG['roles']['manage_tournaments'] as $role_slug) {
	  if (! $role = get_role($role_slug)) continue;
	  $role->add_cap(SARDIN_CONFIG['caps']['tournaments']);
	  foreach ($tournaments_ids as $tournament_id) {
	       $role->add_cap(sprintf(SARDIN_CONFIG['caps']['tournament'],
				      $tournament_id));
	  }
     }
}
// Suppression des permissions globales, pour la désactivation du plugin
function sardin_drop_capabilities() {
     global $wpdb;
     $tournaments_ids = $wpdb->get_col(
	       "SELECT tournament_id
                  FROM ". SARDIN_CONFIG['tables']['tournaments']);

     foreach (SARDIN_CONFIG['roles']['manage_tournaments'] as $role_slug) {
	  if (!$role = get_role($role_slug)) continue;
	  $role->remove_cap(SARDIN_CONFIG['caps']['tournaments']);
	  foreach ($tournaments_ids as $tournament_id) {
	       $role->remove_cap(sprintf(SARDIN_CONFIG['caps']['tournament'],
				      $tournament_id));
	  }
     }
}

// Fonction appelée à création d'un tournoi, donne les droits aux rôles aggréés
function sardin_create_tournament_capability($tournament) {
     $capability = sprintf(SARDIN_CONFIG['caps']['tournament'],
			   $tournament['tournament_id']);
     $current_user = wp_get_current_user();
     foreach (SARDIN_CONFIG['roles']['manage_tournaments'] as $role_slug) {
	  $role = get_role($role_slug);
	  if (!$role) {
	       continue;
	  }
	  $role->add_cap($capability);

	  // On met à jour les rôles de l'utisateur courant pour prendre en
	  // compte la nouvelle permission
	  if (in_array($role_slug, $current_user->roles)) {
	       $current_user->add_role($role_slug);
	  }
     }
}

// Permission par utilisateur
function sardin_give_tournament_capability($tournament, $user=null) {
     $capability = sprintf(SARDIN_CONFIG['caps']['tournament'],
			   $tournament['tournament_id']);
     if (!$user) {
	  $user = wp_get_current_user();
     }
     if (!$user || $user->has_cap($capability)) {
	  return;
     }
     $user->add_cap($capability);
     return true;
}

function sardin_remove_tournament_capability($tournament, $user=null) {
     $capability =  sprintf(SARDIN_CONFIG['caps']['tournament'],
			   $tournament['tournament_id']);
     if (!$user) {
	  $user = wp_get_current_user();
     }
     if (!$user || !$user->has_cap($capability)) {
	  return;
     }
     $user->remove_cap($capability);
     return true;
}

// Pour la désactivation du plugin, on supprime toutes les permissions
// utilisateur
function sardin_drop_users_capabilities() {
     global $wpdb;
     $tournaments_ids = $wpdb->get_col(
	       "SELECT tournament_id
                  FROM ". SARDIN_CONFIG['tables']['tournaments']);
     $users = get_users();
     foreach ($users as $user) {
	  foreach ($tournaments_ids as $tournament_id) {
	       $cap = sprintf(SARDIN_CONFIG['caps']['tournament'],
			      $tournament_id);
	       if ($user->has_cap($cap)) {
		    $user->remove_cap($cap);
	       }
	  }
     }
}
